constexpr int num_iterations = 18;

// #if 1
// #include <cmath>
// 
// #define limestone_min fmin
// #define limestone_max fmax
// #endif

double mandelbrot_kernel(double r) {
    declare_bounds(r, -10, 10);
    double result = 1;

    #pragma unroll
    for(int i = 0; i < num_iterations; i++) {
        r = r+r;

        if (r > 2) {
            result = i;
            return result;
        }

        if(r > 0) {
            __invisible_nop(r);
            __invisible_nop(r + i);
        }
        else {
            __invisible_nop(r*r);
            __invisible_nop(r - i);
        }
    }

    return result;
}
