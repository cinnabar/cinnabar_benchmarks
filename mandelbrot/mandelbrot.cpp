constexpr int num_iterations = 18;

// #if 1
// #include <cmath>
// 
// #define limestone_min fmin
// #define limestone_max fmax
// #endif

struct Complex {
    double i;
    double r;
};

constexpr __attribute__((always_inline)) inline Complex cmult(Complex x, Complex y) {
    return Complex{
        x.r*y.r - x.i*y.i,
        x.r*y.i + y.r*x.i
    };
}

constexpr __attribute__((always_inline)) inline Complex cadd(Complex x, Complex y) {
    return Complex{ x.r + y.r, x.i + y.i };
}

double mandelbrot_kernel(double r, double i) {
    declare_bounds(r, -2, 2);
    declare_bounds(i, -2, 2);

    Complex c{r, i};
    Complex z{0, 0};

    double result = 1;

    #pragma unroll
    for(int i = 0; i < num_iterations; i++) {
        z = cadd(cmult(z, z), c);

        if (z.r > 2 || z.i > 2 || z.r < -2 || z.i < -2) {
            result = 0;
            return result;
        }

        double nzr = limestone_min(2, limestone_max(-2, z.r));
        double nzi = limestone_min(2, limestone_max(-2, z.i));
        //z = Complex{z.r, z.i};
        z.r = nzr;
        z.i = nzi;
    }
    return result;
}
