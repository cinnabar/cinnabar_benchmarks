#include <math.h>
// #include "extmodelfuns.h"

#define max(a,b) a>b ? a : b;
#define min(a,b) a>b ? b : a;

constexpr double TOL = 1e-16;

#ifndef M_PI
#define M_PI 3.141592654
#endif


// Currently, pow is unimplmemented, so we'll place a placeholder here
__attribute__((always_inline)) inline double _pow(double x, double exp) {
    return x * exp;
}

__attribute__((always_inline)) inline double _exp(double x) {
    return x*x;
}

namespace {
    // Simple Euler-forward integration
    __attribute__((always_inline)) inline double
    ApproxInt(double dx, double x0, double Ts)
    {
        return x0 + Ts*dx;
    }

    __attribute__((always_inline)) double robsqrt(double x) {
        // NOTE: This is just to avoid having 0s when calculating division with this
        // value later. In a real use case, this would be done at the use site
        double input = limestone_max(1, limestone_abs(input));
        return limestone_sqrt(input);
    };

    __attribute__((always_inline)) inline double max_fun(double choice_a, double choice_b) {return limestone_max(choice_a, choice_b);};
    __attribute__((always_inline)) inline double min_fun(double choice_a, double choice_b) {return limestone_min(choice_a, choice_b);};


    __attribute__((always_inline)) inline double PSI( double PI, double gamma)
    {
        return robsqrt( ( 2*gamma/(gamma-1) )*( _pow(PI,2/gamma) - _pow(PI,(gamma+1)/gamma) ) );
    }

    __attribute__((always_inline)) inline double
    W_af_fun( double p_amb, double p_af, double plin_af, double H_af, double T_amb )
    {
        double W_af;
        if( p_amb - p_af - plin_af > 0 ) {
            W_af = robsqrt( p_amb/(H_af*T_amb) )*robsqrt(p_amb-p_af);
        } else {
            W_af = robsqrt( p_amb/(H_af*T_amb) )*(p_amb-p_af)/robsqrt(plin_af);
        }
        return W_af;
    }

    __attribute__((always_inline))inline double PSI_th_fun(double p_im, double p_ic, double gamma_air, double PIli_th)
    {
        double PI = p_im/p_ic;
        double PI_th;
        double PSI_th;

        if( PI < PIli_th ) {
            PI_th = limestone_max( PI, _pow( 2/(gamma_air+1),gamma_air/(gamma_air-1) ) );
            PSI_th = robsqrt( ( 2*gamma_air/(gamma_air-1) )*( _pow(PI_th,2/gamma_air)-_pow(PI_th,(gamma_air+1)/gamma_air) ) );
        } else {
            PI_th = limestone_max( PIli_th, _pow( 2/(gamma_air+1),gamma_air/(gamma_air-1) ) );
            PSI_th = robsqrt( ( 2*gamma_air/(gamma_air-1) )*( _pow( PI_th,2/gamma_air )-_pow( PI_th,(gamma_air+1)/gamma_air ) ) )/(1-PIli_th)*(1 - PI);
        }
        return PSI_th;
    }

    __attribute__((always_inline)) inline double PI_c_fun(double p_c, double p_af)
    {
        
        if( p_c/p_af>1 ) {
            return p_c/p_af;
        } else {
            return 1 - TOL;
        }
    }
            
    __attribute__((always_inline)) inline double PHI_model_fun(double K1, double K2, double PSI_c)
    {
        double PHI_model;
        if ( (1-K1*PSI_c*PSI_c)/K2 > 0 ) {
            PHI_model = robsqrt((1-K1*PSI_c*PSI_c)/K2);
        } else {
            PHI_model = 0;
        }
        return PHI_model;
    }

    __attribute__((always_inline)) inline double W_c_fun(double PI_cnolim, double p_af, double U_c, double D_c, double T_af, double R_air, double PHI_model, double fix_gain)
    {
        double Wc;
        if( PI_cnolim - 1 >= 0 ) {
            Wc =    ( p_af*U_c*M_PI*D_c*D_c/(T_af*2*R_air) )*PHI_model;
        } else {
            Wc = ( p_af*U_c*M_PI*D_c*D_c/(T_af*2*R_air) )*PHI_model*(1-(2-2*PI_cnolim)) + fix_gain*(2-2*PI_cnolim);
        }
        return Wc;
    }


    __attribute__((always_inline)) inline double eta_c_fun(double eta_cmax, double eta_cmin, double W_ccorr, double W_ccorrmax, double PI_c, double PI_cmax, double Q_c11, double Q_c12, double Q_c22) 
    {
        double X_c1, X_c2;
        double eta_c_comparison;

        X_c1 = W_ccorr-W_ccorrmax; 

        if( PI_c > 1 ) {
            X_c2 = robsqrt(PI_c-1)+1-PI_cmax;
        } else {
            X_c2 = 1-PI_cmax;
        }
        
        eta_c_comparison = eta_cmax - ( X_c1*X_c1*Q_c11 + 2*X_c1*X_c2*Q_c12 + X_c2*X_c2*Q_c22 );
        
        if( eta_c_comparison >= 1 ) {
            return 1 - TOL;
        } else if( eta_cmin < eta_c_comparison && eta_c_comparison < 1 ) {
            return eta_c_comparison;
        } else {
            return eta_cmin;
        } 
    }

    __attribute__((always_inline)) inline double W_ic_fun(double p_c, double p_ic, double plin_intercooler, double H_intercooler, double T_c)
    {
        if( p_c-p_ic-plin_intercooler > 0 ) {
            return robsqrt(p_c/(H_intercooler*T_c))*robsqrt(p_c-p_ic);
        } else {
            return robsqrt(p_c/(H_intercooler*T_c))*(p_c-p_ic)/robsqrt(plin_intercooler);
        }
    }

    __attribute__((always_inline)) inline double Tflow_wg_fun(double p_t, double p_em, double T_em, double T_t)
    {
        if( p_em < p_t ) {
            return T_t;
        } else {
            return T_em;
        }
    }

    __attribute__((always_inline)) inline double W_es_fun(double p_amb, double p_t, double plin_exh, double H_exhaust, double T_t)
    {
        if( plin_exh > p_t-p_amb ) {
            return robsqrt( p_t/(H_exhaust*T_t) )*(p_t-p_amb)/robsqrt(plin_exh);
        } else {
            return robsqrt( p_t/(H_exhaust*T_t) )*robsqrt(p_t-p_amb);
        }
    }

    __attribute__((always_inline)) inline double PI_wg_fun( double p_t, double p_em, double gamma_air )
    {
        double x = _pow( 2/(gamma_air+1) , gamma_air/(gamma_air-1) );
        if( -p_t/p_em + x > 0 ) {
            return x;
        } else {
            return p_t/p_em;
        }
    } 

    __attribute__((always_inline)) inline double PSIli_wg_fun( double PI_wg, double PIli_wg, double gamma_exh)
    {
        double PI;
        double x = _pow( 2/(gamma_exh+1), gamma_exh/(gamma_exh - 1) );
        if( PIli_wg - PI_wg > 0 ) {
            PI = limestone_max(PI_wg, x);	
            return PSI(PI,gamma_exh);
        } else {
            PI = limestone_max(PIli_wg, x);	
            return PSI(PI,gamma_exh)*(1-PI_wg)/(1-PIli_wg);
        }
    }

    __attribute__((always_inline)) inline double PI_t_fun( double p_t, double p_em )
    {
        double x = p_t/p_em;
        if( x > 1 ) {
            return 1 - TOL;
        } else {
            return x;
        }
    } 

    __attribute__((always_inline)) inline double W_t_fun(double p_em, double k1, double PI_t, double k2, double T_em)
    {
        double x = limestone_max( TOL , 1-_pow(PI_t, k2) );
        return ( p_em*0.001*k1*robsqrt(x) )/robsqrt(T_em);
    }

    __attribute__((always_inline)) inline double eta_t_fun( double k3, double BSR, double k4, double eta_min )
    {
        double eta_t = k3*(1-((BSR - k4)/k4)*((BSR - k4)/k4));
        
        if (eta_t > k3) {
            return k3;
        } else if (eta_t < eta_min) {
            return eta_min;
        } else { 
            return eta_t;
        } 
    }

    __attribute__((always_inline)) inline double Tq_t_fun(double gamma_eg, double cp_eg, double eta_t, double W_t, double T_em, double PI_t, double omega_tc)
    {
        double x = limestone_max(0, 1-_pow(PI_t, (gamma_eg-1)/gamma_eg) );
        return cp_eg*eta_t*W_t*T_em*x/omega_tc;
    }




    // These are placeholder values
    constexpr double a_omega[7] = {
        0,
        102515623.07366969,
        9366401.458247459,
        78647878.3926235,
        53782091.40362074,
        46559598.548757285,
        106900665.763039
    };
    constexpr double b_omega[7] = {
        -1144332.45600859,
        -194324.09978921397,
        -426767.9623230614,
        -1096715.2135825262,
        -286132.12951784534,
        -311914.8446650886,
        0
    };
    constexpr double omega_breakpoints[7] = {
        0,
        14864.841926144087,
        3037.44655279657,
        910.699533310967,
        5906.51484105571,
        11368.2938515065,
        19969.5337025435
    };


    __attribute__((always_inline)) inline double a_eta_t_fun(double omega_tc)
    {
        if( omega_tc < 0) return a_omega[0];

#pragma unroll
        for( int ii = 1;ii <= 6; ii++ )
        {
            if( omega_tc < omega_breakpoints[ii])
            {
                auto div_val = limestone_max(1, (omega_breakpoints[ii] - omega_breakpoints[ii-1]));
                return a_omega[ii-1] + (a_omega[ii]-a_omega[ii-1])/div_val*(omega_tc-omega_breakpoints[ii-1]);
            }
        }

        return a_omega[6];
    }

    __attribute__((always_inline)) inline double b_eta_t_fun(double omega_tc)
    {

        if( omega_tc < 0 ) return b_omega[0];

#pragma unroll
        for( int ii = 1;ii <= 6; ii++ )
        {
            if( omega_tc < omega_breakpoints[ii])
            {
                auto div_val = limestone_max(1, (omega_breakpoints[ii] - omega_breakpoints[ii-1]));
                return b_omega[ii-1] + (b_omega[ii]-b_omega[ii-1])/div_val*(omega_tc-omega_breakpoints[ii-1]);
            }
        }

        return b_omega[6];
    }
}
