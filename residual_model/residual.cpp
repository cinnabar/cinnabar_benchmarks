// #include "mex.h"

#include <math.h>

// External function headers
#include "extmodelfuns.cpp"

// INTRESBATCH_114_21_C Sequential residual generator for model 'Engine model' (batch version)
// Causality: int
//
// Structurally sensitive to faults: fp_af, fw_af, fw_c, fc_vol, fw_t, fx_th, fyp_im, fyp_ic
//
// Example of basic usage:
//     Let z be the observations and N the number of samples, then
//     the residual generator can be simulated by:
//
//     r = IntResBatch_114_21_c( z, state, params, 1/fs );
//
//     The observations z must be a MxN matrix where M is the number
//     of known signals and N the number of samples.
//
//     State is a structure with the state of the residual generator.
//     The state has fieldnames: wg_pos, m_c, m_t, m_em, m_af, omega_tc, T_t, T_em, T_im, T_c, T_af, m_ic, T_ic

// File generated 07-Dec-2018 11:03:33

typedef struct {
    double wg_pos;
    double m_c;
    double m_t;
    double m_em;
    double m_af;
    double omega_tc;
    double T_t;
    double T_em;
    double T_im;
    double T_c;
    double T_af;
    double m_ic;
    double T_ic;
} ResState;

typedef struct {
    double H_af;
    double plin_af;
    double H_exhaust;
    double plin_exh;
    double H_intercooler;
    double plin_intercooler;
    double PIli_th;
    double gamma_air;
    double PIli_wg;
    double gamma_exh;
    double R_air;
    double cp_air;
    double V_af;
    double V_c;
    double V_ic;
    double V_im;
    double R_exh;
    double cp_exh;
    double lambda;
    double n_r;
    double r_c;
    double V_D;
    double CEva_cool;
    double D_c;
    double K1;
    double K2;
    double fix_gain;
    double eta_cmax;
    double eta_cmin;
    double T_std;
    double k1;
    double k2;
    double cp_eg;
    double gamma_eg;
    double tau_wg;
    double h_tot;
    double Amax;
    double A_0;
    double A_1;
    double A_2;
    double J_tc;
    double xi_fric_tc;
    double V_em;
    double V_t;
    double p_std;
    double a1;
    double a2;
    double a3;
    double a4;
    double a5;
    double Q_c11;
    double Q_c12;
    double Q_c22;
    double Cd;
    double PI_cmax;
    double cv_exh;
    double cv_air;
    double W_ccorrmax;
    double A_em;
    double K_t;
    double T0;
    double Cic_1;
    double Cic_2;
    double Cic_3;
    double TOL;
} Parameters;

ResState
residual(
    // Previous state
    double wg_pos,
    double m_c,
    double m_t,
    double m_em,
    double m_af,
    double omega_tc,
    double T_t,
    double T_em,
    double T_im,
    double T_c,
    double T_af,
    double m_ic,
    double T_ic,
    // Known variables (called z in original code)
    const double y_p_ic,
    const double y_p_im,
    const double y_omega_e,
    const double y_alpha_th,
    const double y_u_wg,
    const double y_wfc,
    const double y_T_amb,
    const double y_p_amb,
    // Used to be in the parameters struct
    const double H_af,
    const double plin_af,
    const double H_exhaust,
    const double plin_exh,
    const double H_intercooler,
    const double plin_intercooler,
    const double PIli_th,
    const double gamma_air,
    const double PIli_wg,
    const double gamma_exh,
    const double R_air,
    const double cp_air,
    const double V_af,
    const double V_c,
    const double V_ic,
    const double V_im,
    const double R_exh,
    const double cp_exh,
    const double lambda,
    const double n_r,
    const double r_c,
    const double V_D,
    const double CEva_cool,
    const double D_c,
    const double K1,
    const double K2,
    const double fix_gain,
    const double eta_cmax,
    const double eta_cmin,
    const double T_std,
    const double k1,
    const double k2,
    const double cp_eg,
    const double gamma_eg,
    const double tau_wg,
    const double h_tot,
    const double Amax,
    const double A_0,
    const double A_1,
    const double A_2,
    const double J_tc,
    const double xi_fric_tc,
    const double V_em,
    const double V_t,
    const double p_std,
    const double a1,
    const double a2,
    const double a3,
    const double a4,
    const double a5,
    const double Q_c11,
    const double Q_c12,
    const double Q_c22,
    const double Cd,
    const double PI_cmax,
    const double cv_exh,
    const double cv_air,
    const double W_ccorrmax,
    const double A_em,
    const double K_t,
    const double T0,
    const double Cic_1,
    const double Cic_2,
    const double Cic_3,
    const double TOL,
    double Ts
)
{

    ResState state;
    double    *matVarPr;

    // Parameters
    /*
    double H_af = params.H_af;
    double plin_af = params.plin_af;
    double H_exhaust = params.H_exhaust;
    double plin_exh = params.plin_exh;
    double H_intercooler = params.H_intercooler;
    double plin_intercooler = params.plin_intercooler;
    double PIli_th = params.PIli_th;
    double gamma_air = params.gamma_air;
    double PIli_wg = params.PIli_wg;
    double gamma_exh = params.gamma_exh;
    double R_air = params.R_air;
    double cp_air = params.cp_air;
    double V_af = params.V_af;
    double V_c = params.V_c;
    double V_ic = params.V_ic;
    double V_im = params.V_im;
    double R_exh = params.R_exh;
    double cp_exh = params.cp_exh;
    double lambda = params.lambda;
    double n_r = params.n_r;
    double r_c = params.r_c;
    double V_D = params.V_D;
    double CEva_cool = params.CEva_cool;
    double D_c = params.D_c;
    double K1 = params.K1;
    double K2 = params.K2;
    double fix_gain = params.fix_gain;
    double eta_cmax = params.eta_cmax;
    double eta_cmin = params.eta_cmin;
    double T_std = params.T_std;
    double k1 = params.k1;
    double k2 = params.k2;
    double cp_eg = params.cp_eg;
    double gamma_eg = params.gamma_eg;
    double tau_wg = params.tau_wg;
    double h_tot = params.h_tot;
    double Amax = params.Amax;
    double A_0 = params.A_0;
    double A_1 = params.A_1;
    double A_2 = params.A_2;
    double J_tc = params.J_tc;
    double xi_fric_tc = params.xi_fric_tc;
    double V_em = params.V_em;
    double V_t = params.V_t;
    double p_std = params.p_std;
    double a1 = params.a1;
    double a2 = params.a2;
    double a3 = params.a3;
    double a4 = params.a4;
    double a5 = params.a5;
    double Q_c11 = params.Q_c11;
    double Q_c12 = params.Q_c12;
    double Q_c22 = params.Q_c22;
    double Cd = params.Cd;
    double PI_cmax = params.PI_cmax;
    double cv_exh = params.cv_exh;
    double cv_air = params.cv_air;
    double W_ccorrmax = params.W_ccorrmax;
    double A_em = params.A_em;
    double K_t = params.K_t;
    double T0 = params.T0;
    double Cic_1 = params.Cic_1;
    double Cic_2 = params.Cic_2;
    double Cic_3 = params.Cic_3;
    double TOL = params.TOL;
    */

    declare_bounds(wg_pos, 0.9, 1);
    declare_bounds(m_c, 0.9, 1);
    declare_bounds(m_t, 0.9, 1);
    declare_bounds(m_em, 0.9, 1);
    declare_bounds(m_af, 0.9, 1);
    declare_bounds(omega_tc, 0.9, 1);
    declare_bounds(T_t, 0.9, 1);
    declare_bounds(T_em, 0.9, 1);
    declare_bounds(T_im, 0.9, 1);
    declare_bounds(T_c, 0.9, 1);
    declare_bounds(T_af, 0.9, 1);
    declare_bounds(m_ic, 0.9, 1);
    declare_bounds(T_ic, 0.9, 1);
    // Known variables (called z in original code)
    declare_bounds( y_p_ic, 0.9, 1 );
    declare_bounds(y_p_im, 0.9, 1);
    declare_bounds(y_omega_e, 0.9, 1);
    declare_bounds(y_alpha_th, 0.9, 1);
    declare_bounds(y_u_wg, 0.9, 1);
    declare_bounds(y_wfc, 0.9, 1);
    declare_bounds(y_T_amb, 0.9, 1);
    declare_bounds(y_p_amb, 0.9, 1);
    // Used to be in the parameters struct
    declare_bounds(H_af, 0.9, 1);
    declare_bounds(plin_af, 0.9, 1);
    declare_bounds(H_exhaust, 0.9, 1);
    declare_bounds(plin_exh, 0.9, 1);
    declare_bounds(H_intercooler, 0.9, 1);
    declare_bounds(plin_intercooler, 0.9, 1);
    declare_bounds(PIli_th, 0.9, 0.8);
    declare_bounds(gamma_air, 2.1, 3);
    declare_bounds(PIli_wg, 0.9, 0.9);
    declare_bounds(gamma_exh, 0.9, 1);
    declare_bounds(R_air, 0.9, 1);
    declare_bounds(cp_air, 0.9, 1);
    declare_bounds(V_af, 0.9, 1);
    declare_bounds(V_c, 0.9, 1);
    declare_bounds(V_ic, 0.9, 1);
    declare_bounds(V_im, 0.9, 1);
    declare_bounds(R_exh, 0.9, 1);
    declare_bounds(cp_exh, 0.9, 1);
    declare_bounds(lambda, 0.9, 1);
    declare_bounds(n_r, 0.9, 1);
    declare_bounds(r_c, 1.1, 2);
    declare_bounds(V_D, 0.9, 1);
    declare_bounds(CEva_cool, 0.9, 1);
    declare_bounds(D_c, 0.9, 1);
    declare_bounds(K1, 0.9, 1);
    declare_bounds(K2, 0.9, 1);
    declare_bounds(fix_gain, 0.9, 1);
    declare_bounds(eta_cmax, 0.9, 1);
    declare_bounds(eta_cmin, 0.9, 1);
    declare_bounds(T_std, 0.9, 1);
    declare_bounds(k1, 0.9, 1);
    declare_bounds(k2, 0.9, 1);
    declare_bounds(cp_eg, 0.9, 1);
    declare_bounds(gamma_eg, 0.9, 1);
    declare_bounds(tau_wg, 0.9, 1);
    declare_bounds(h_tot, 0.9, 1);
    declare_bounds(Amax, 0.9, 1);
    declare_bounds(A_0, 0.9, 1);
    declare_bounds(A_1, 0.9, 1);
    declare_bounds(A_2, 0.9, 1);
    declare_bounds(J_tc, 0.9, 1);
    declare_bounds(xi_fric_tc, 0.9, 1);
    declare_bounds(V_em, 0.9, 1);
    declare_bounds(V_t, 0.9, 1);
    declare_bounds(p_std, 0.9, 1);
    declare_bounds(a1, 0.9, 1);
    declare_bounds(a2, 0.9, 1);
    declare_bounds(a3, 0.9, 1);
    declare_bounds(a4, 0.9, 1);
    declare_bounds(a5, 0.9, 1);
    declare_bounds(Q_c11, 0.9, 1);
    declare_bounds(Q_c12, 0.9, 1);
    declare_bounds(Q_c22, 0.9, 1);
    declare_bounds(Cd, 0.9, 1);
    declare_bounds(PI_cmax, 0.9, 1);
    declare_bounds(cv_exh, 0.9, 1);
    declare_bounds(cv_air, 0.9, 1);
    declare_bounds(W_ccorrmax, 0.9, 1);
    declare_bounds(A_em, 0.9, 1);
    declare_bounds(K_t, 0.9, 1);
    declare_bounds(T0, 0.9, 1);
    declare_bounds(Cic_1, 0.9, 1);
    declare_bounds(Cic_2, 0.9, 1);
    declare_bounds(Cic_3, 0.9, 1);
    declare_bounds(TOL, 0.9, 1);
    declare_bounds(Ts, 0.9, 1);



    // Declare residual generator variables
    double W_af;
    double p_t;
    double W_es;
    double p_ic;
    double W_ic;
    double W_th;
    double Aeff_th;
    double W_wg;
    double Aeff_wg;
    double p_af;
    double W_c;
    double T_cout;
    double p_c;
    double T_imcr;
    double p_im;
    double W_e;
    double T_ti;
    double p_em;
    double dh_is;
    double W_twg;
    double T_turb;
    double alpha_th;
    double omega_e;
    double Tq_c;
    double eta_c;
    double PSI_c;
    double PI_c;
    double W_t;
    double Tq_t;
    double eta_t;
    double PI_t;
    double u_wg;
    double T_e;
    double T_amb;
    double p_amb;
    double PSI_th;
    double PI_wg;
    double PSIli_wg;
    double T_fwd_flow_ic;
    double m_im;
    double PI_cnolim;
    double U_c;
    double PHI_model;
    double W_ccorr;
    double C_eta_vol;
    double T_in;
    double eta_vol;
    double W_ac;
    double W_fc;
    double T_tout;
    double Tflow_wg;
    double dmdt_af;
    double dTdt_af;
    double dmdt_c;
    double dTdt_c;
    double dmdt_ic;
    double dTdt_ic;
    double dTdt_im;
    double dmdt_em;
    double dTdt_em;
    double dmdt_t;
    double dTdt_t;
    double domegadt_tc;
    double dwgdt_pos;

    // Residual generator body
    W_fc = y_wfc; // e92
    p_amb = y_p_amb; // e94
    T_amb = y_T_amb; // e93
    u_wg = y_u_wg; // e91
    dwgdt_pos = (u_wg-wg_pos)/tau_wg; // e79
    omega_e = y_omega_e; // e89
    alpha_th = y_alpha_th; // e90
    p_im = y_p_im; // e86
    C_eta_vol = a5+(a4*max_fun(p_im,2.5E4))/1.0E3+(a1*_pow(max_fun(p_im,2.5E4),4.0))/1.0E12+(a2*_pow(max_fun(p_im,2.5E4),3.0))/1.0E9+(a3*_pow(max_fun(p_im,2.5E4),2.0))/1.0E6; // e46
    Aeff_wg = Amax*Cd*wg_pos; // e78
    Aeff_th = A_0+(A_1*3.141592653589793*(alpha_th*(4.3E1/5.0E1)+4.0))/1.8E2+(A_2*(3.141592653589793*3.141592653589793)*_pow(alpha_th*(4.3E1/5.0E1)+4.0,2.0))/3.24E4; // e42
    p_ic = y_p_ic; // e85
    PSI_th = PSI_th_fun(p_im,p_ic,gamma_air,PIli_th); // e4
    T_in = T_im-CEva_cool*(1.0/lambda-1.0); // e44
    U_c = (D_c*omega_tc)/2.0; // e66
    m_im = (V_im*p_im)/(R_air*T_im); // e26
    p_em = (R_exh*T_em*m_em)/V_em; // e31
    eta_vol = (C_eta_vol*T_im*(r_c-_pow(p_em/p_im,1.0/gamma_exh)))/(T_in*(r_c-1.0)); // e45
    W_ac = (V_D*eta_vol*omega_e*p_im)/(R_air*T_im*n_r*3.141592653589793*2.0); // e43
    W_e = W_ac+W_fc; // e57
    T_e = T0+K_t*W_e; // e58
    T_ti = T_amb-_exp((A_em*h_tot*-4.0)/limestone_max(1, W_e*cp_exh))*(T_amb-T_e); // e59
    T_imcr = T_ic; // e6
    p_c = (R_air*T_c*m_c)/V_c; // e16
    p_af = (R_air*T_af*m_af)/V_af; // e11
    PI_cnolim = p_c/p_af; // e65
    PI_c = PI_c_fun(p_c,p_af); // e64
    PSI_c = T_af*1.0/(U_c*U_c)*cp_air*(_pow(PI_c,(gamma_air-1.0)/gamma_air)-1.0)*2.0; // e63
    PHI_model = PHI_model_fun(K1,K2,PSI_c); // e62
    W_c = W_c_fun(PI_cnolim,p_af,U_c,D_c,T_af,R_air,PHI_model,fix_gain); // e61
    W_ccorr = (W_c*p_std*limestone_sqrt(T_af/T_std))/p_af; // e69
    eta_c = eta_c_fun(eta_cmax,eta_cmin,W_ccorr,W_ccorrmax,PI_c,PI_cmax,Q_c11,Q_c12,Q_c22); // e68
    double T_cout_div = limestone_max(1, eta_c+1);
    T_cout = T_af*((_pow(PI_c,(gamma_air-1.0)/gamma_air)-1.0)/T_cout_div); // e67
    Tq_c = -(W_c*cp_air*(T_af-T_cout))/omega_tc; // e60
    W_th = Aeff_th*PSI_th*p_ic*1.0/limestone_sqrt(R_air*T_ic); // e5
    dTdt_im = -(R_air*(T_im*W_ac-T_imcr*W_th)+W_th*cv_air*(T_im-T_imcr))/(cv_air*m_im); // e28
    W_ic = W_ic_fun(p_c,p_ic,plin_intercooler,H_intercooler,T_c); // e1
    T_fwd_flow_ic = max_fun(T_amb,T_c-max_fun(TOL,-(T_amb-T_c)*(Cic_1+(Cic_2*(T_amb+T_c))/2.0+Cic_3*W_ic))); // e41
    dTdt_ic = (R_air*(T_fwd_flow_ic*W_ic-T_ic*W_th)-W_ic*cv_air*(T_ic-T_fwd_flow_ic))/(cv_air*m_ic); // e23
    dmdt_ic = W_ic-W_th; // e22
    dTdt_c = (R_air*(T_cout*W_c-T_c*W_ic)-W_c*cv_air*(T_c-T_cout))/(cv_air*m_c); // e18
    dmdt_c = W_c-W_ic; // e17
    p_t = (R_exh*T_t*m_t)/V_t; // e36
    PI_t = PI_t_fun(p_t,p_em); // e75
    dh_is = -T_em*cp_exh*(_pow(PI_t,(gamma_exh-1.0)/gamma_exh)-1.0); // e73
    W_t = W_t_fun(p_em,k1,PI_t,k2,T_em); // e70
    eta_t = max_fun(TOL,min_fun(-TOL+1.0,(b_eta_t_fun(omega_tc)+(limestone_sqrt(T_em)*W_t*a_eta_t_fun(omega_tc)*1.0E3)/p_em)/dh_is)); // e72
    T_tout = T_em*(eta_t*(_pow(PI_t,(gamma_eg-1.0)/gamma_eg)-1.0)+1.0); // e74
    Tq_t = Tq_t_fun(gamma_eg,cp_eg,eta_t,W_t,T_em,PI_t,omega_tc); // e71
    domegadt_tc = -(Tq_c-Tq_t+omega_tc*xi_fric_tc)/J_tc; // e83
    Tflow_wg = Tflow_wg_fun(p_t,p_em,T_em,T_t); // e10
    PI_wg = PI_wg_fun(p_t,p_em,gamma_air); // e7
    PSIli_wg = PSIli_wg_fun(PI_wg,PIli_wg,gamma_exh); // e8
    W_wg = Aeff_wg*PSIli_wg*p_em*1.0/limestone_sqrt(R_exh*Tflow_wg); // e9
    W_twg = W_t+W_wg; // e77
    dTdt_em = (R_exh*(T_ti*W_e-T_em*W_twg)-W_e*cv_exh*(T_em-T_ti))/(cv_exh*m_em); // e33
    dmdt_em = W_e-W_twg; // e32
    T_turb = (T_tout*W_t+Tflow_wg*W_wg)/limestone_max(1, W_t+W_wg); // e76
    W_es = W_es_fun(p_amb,p_t,plin_exh,H_exhaust,T_t); // e3
    dTdt_t = -(R_exh*(T_t*W_es-T_turb*W_twg)+W_twg*cv_exh*(T_t-T_turb))/(cv_exh*m_t); // e38
    dmdt_t = -W_es+W_twg; // e37
    W_af = W_af_fun(p_amb,p_af,plin_af,H_af,T_amb); // e2
    dTdt_af = (R_air*(T_amb*W_af-T_af*W_c)-W_af*cv_air*(T_af-T_amb))/(cv_air*m_af); // e13
    dmdt_af = W_af-W_c; // e12
     
    // r[0]=p_ic-(R_air*T_ic*m_ic)/V_ic; // e21

    // Update integrator variables
    double o_wg_pos = ApproxInt(dwgdt_pos,wg_pos,Ts); // e80
    double o_m_c = ApproxInt(dmdt_c,m_c,Ts); // e19
    double o_m_t = ApproxInt(dmdt_t,m_t,Ts); // e39
    double o_m_em = ApproxInt(dmdt_em,m_em,Ts); // e34
    double o_m_af = ApproxInt(dmdt_af,m_af,Ts); // e14
    double o_omega_tc = ApproxInt(domegadt_tc,omega_tc,Ts); // e84
    double o_T_t = ApproxInt(dTdt_t,T_t,Ts); // e40
    double o_T_em = ApproxInt(dTdt_em,T_em,Ts); // e35
    double o_T_im = ApproxInt(dTdt_im,T_im,Ts); // e30
    double o_T_c = ApproxInt(dTdt_c,T_c,Ts); // e20
    double o_T_af = ApproxInt(dTdt_af,T_af,Ts); // e15
    double o_m_ic = ApproxInt(dmdt_ic,m_ic,Ts); // e24
    double o_T_ic = ApproxInt(dTdt_ic,T_ic,Ts); // e25

    // Update state variables
    state.wg_pos = o_wg_pos;
    state.m_c = o_m_c;
    state.m_t = o_m_t;
    state.m_em = o_m_em;
    state.m_af = o_m_af;
    state.omega_tc = o_omega_tc;
    state.T_t = o_T_t;
    state.T_em = o_T_em;
    state.T_im = o_T_im;
    state.T_c = o_T_c;
    state.T_af = o_T_af;
    state.m_ic = o_m_ic;
    state.T_ic = o_T_ic;

    return state;
}

