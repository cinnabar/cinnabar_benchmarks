#!/usr/bin/python3

# Script for quickly scrambling array elements while maintaining the min and max values.
#
# Best used with watchexec and xclip: watchexec -e py -- 'python3 map_scrambler.py | xclip -selection clipboard'

import random

from to_scramble import input

def scramble(input):
    max_val = max(input)
    min_val = min(input)

    return [min_val] + list(map(lambda x: random.uniform(min_val, max_val), range(len(input)-2))) + [max_val]

for val in scramble(input):
    print(val, end=",\n")
print()
