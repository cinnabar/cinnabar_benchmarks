#!/bin/bash

# Locates all tar files in the build directory and scps them to $DESTINATION

# FOREIGN="home"
FOREIGN="liu"
USERNAME="frask53"
DESTINATION="/home/${USERNAME}/Documents/phd/synth/batch"

tar_files=$(find build -name '*.tar.gz')

# echo sending "$tar_files"
rsync -v ${tar_files} "$USERNAME@$FOREIGN:$DESTINATION"
