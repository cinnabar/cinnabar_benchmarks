#!/bin/sh

set -e pipefail

sort naive > naive_sorted
sort smart > smart_sorted

prettydiff naive_sorted smart_sorted

rm naive_sorted smart_sorted
