#!/usr/bin/env rust-script

//! ```cargo
//! [dependencies]
//! serde = "1.0.117"
//! serde_derive = "1.0.117"
//! anyhow = "1.0.34"
//! toml = "0.5.7"
//! structopt = "0.3.21"
//! ```

use std::fs;
use std::process::Command;
use std::path::PathBuf;

use anyhow::{anyhow, Result, Context};
use serde_derive::Serialize;
use structopt::StructOpt;

#[derive(Debug, Serialize)]
struct Metadata {
    bench_version: String,
    cinnabar_version: String,
    limestone_version: String,
    description: String,

    word_length: String,
    opt: String,
    model: String,
    opt_flags: String,
}

pub fn run_command_with_output(cmd: &str, args: &[&str]) -> Result<String> {
    let output = Command::new(cmd)
        .args(args)
        .output()?;

    if !output.status.success() {
        return Err(anyhow!("{} {:?} failed with code {:?}", cmd, args, output.status.code()));
    }

    Ok(String::from_utf8_lossy(&output.stdout).trim_end().to_string())
}

/// Runs a command which inherits stdin and stdout and returns an error if the command failed
pub fn run_command(cmd: &str, args: &[&str]) -> Result<()> {
    let status = Command::new(cmd)
        .args(args)
        .status()?;

    if !status.success() {
        return Err(anyhow!("{} {:?} failed with code {:?}", cmd, args, status.code()));
    }

    Ok(())
}

impl Metadata {
    pub fn new(opt: Opt) -> Self {
        let current_dir = std::env::current_dir()
            .expect("failed to get current dir");
        std::env::set_current_dir("cinnabar/mercurous/limestone")
            .expect("failed to cd to limestone");
        let limestone_version = run_command_with_output("git", &["rev-parse", "HEAD"])
                    .expect("Failed to get limestone version");
        std::env::set_current_dir(current_dir)
            .expect("failed to reset working dir");
        Self {
            bench_version:
                run_command_with_output("git", &["rev-parse", "HEAD"])
                    .expect("Failed to get thesis_private version"),
            cinnabar_version:
                run_command_with_output("git", &["rev-parse", "@:./cinnabar"])
                    .expect("Failed to get cinnabar version"),
            limestone_version,
            description: opt.description,
            word_length: opt.word_length,
            opt: opt.opt_level,
            opt_flags: opt.opt_flags,
            model: opt.model,
        }
    }
}


#[derive(StructOpt, Clone)]
struct Opt {
    #[structopt(short="o")]
    outfile: PathBuf,
    /// Description of the run
    description: String,
    #[structopt(long="wl")]
    word_length: String,
    #[structopt(long="opt")]
    opt_level: String,
    #[structopt(long="opt-flags")]
    opt_flags: String,
    #[structopt(long="model")]
    model: String
}

fn main() -> Result<()> {
    let opt = Opt::from_args();

    // Generate a metadata file
    let toml_result = toml::to_string(&Metadata::new(opt.clone()))
        .context("Failed to encode metadata as toml")?;

    let metadata_file = opt.outfile;
    fs::write(metadata_file, &toml_result.as_bytes())
        .context("failed to write metadata")?;

    Ok(())
}
