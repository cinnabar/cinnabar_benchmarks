$(shell cd cinnabar/mercurous && cargo build)
# DEBUGGER=valgrind --track-origins=yes --tool=memcheck
DEBUGGER=
MERC_EXE=cinnabar/mercurous/target/debug/cinnabar
BUILD_DIR=build

# BENCH_FILES= hev_model/pt_model.cpp
BENCH_FILES=# PneumaticValve/pneumatic_valve.cpp \
# 			residual_model/residual.cpp \
# 			hev_model/pt_model.cpp \

# BENCH_FILES=PneumaticValve/pneumatic_valve.cpp \

# BENCH_FILES += residual_model/residual.cpp
# BENCH_FILES += mandelbrot/mandelbrot.cpp
# BENCH_FILES += mandelbrot_structure/mandelbrot_structure.cpp
# BENCH_FILES += mandelbrot_structure2/mandelbrot_structure2.cpp
# BENCH_FILES += hev_model/pt_model.cpp
BENCH_FILES += PneumaticValve/pneumatic_valve.cpp
# BENCH_FILES += paper_example/paper_example.cpp
# BENCH_FILES += unrolled_loop/unrolled_loop.cpp


# BENCH_FILES=mandelbrot/mandelbrot.cpp
	

#			hev_model/pt_model.cpp \

# 			hev_model_reduced/pt_model_reduced.cpp \
# 			simple/simple.c
# BENCH_FILES=hev_model_parted/pt_model_parted.cpp
#BENCH_FILES=part5_cond/part5_cond.cpp
# BENCH_FILES=chained_if/chained_if.cpp

BUILD_SUFFIX=$(OPT)_$(WL)_$(DESCRIPTION)_build

OUTDIRS=$(patsubst %.cpp, ${BUILD_DIR}/%_$(BUILD_SUFFIX), $(BENCH_FILES))


ifeq ($(OPT_OPTS),)
	OPT_OPTS=--unsafe-fp --no-slp-vectorization
endif

ifeq ($(CPP_DATA_TYPE),)
	CPP_DATA_TYPE=fp
endif


all: $(OUTDIRS)

merc: ${MERC_EXE} | build_merc

build_merc:
	cd cinnabar/mercurous && cargo build


${BUILD_DIR}/%_$(BUILD_SUFFIX):
ifeq ($(OPT),)
OPT_ARG=
OPT_META="none"
else
OPT_ARG=--opt $(OPT)
OPT_META=$(OPT)
endif


# A hack for allowing the use of source files in multiple languages with the same
# underlying make rule. The %.source file contains the location of the actual source
# code to use, and these rules build that file. The ll rule also compiles the source
# to bitcode before proceding
_source/${BUILD_SUFFIX}/%.source: %.cpp
	@mkdir -p ${@D}
	@echo $< > $@

_source/${BUILD_SUFFIX}/%.source: %.ll
	@mkdir -p ${@D}
	@llvm-as $< -o $<.bc
	@echo $<.bc > $@

${BUILD_DIR}/%_$(BUILD_SUFFIX): _source/${BUILD_SUFFIX}/%.source merc Makefile
ifeq ($(WL),)
	@echo -e "[\033[0;31mERROR\033[0m] \$$WL (Word length) not set"
	exit 1;
endif
	@echo -e "[\033[0;34mmercurous\033[0m] generating code for $<"
	@mkdir -p $@
	${MERC_EXE} \
		$$(cat $<) \
		-o $@ \
		-c _module_ \
		--cinnabar-graph cinnabar.gv \
		--merged-cinnabar-graph merged_cinnabar.gv \
		--ident-map idents.json \
		--limestone-graph limestone.gv \
		--cinnabar-root cinnabar \
		--block-graph block.gv \
		-h cinnabar/mercurous/limestone/include/liblimestone.hpp \
		--cpp-data-type $(CPP_DATA_TYPE)\
		$(OPT_ARG) \
		$(OPT_OPTS) \
		-w $(WL) \
		# --verify-samples 1000
	@echo -e "[\033[0;34mverilator\033[0m] building verilog"
	# @verilator -Wno-fatal --lint-only -Wall $@/_module_/*.v
	@echo -e "[\033[0;34mmetadata\033[0m] generating metadata file"
	@./gen_metadata.rs -o -- \
		-o $@/metadata.toml \
		--opt $(OPT_META) \
		--wl $(WL) "Makefile generated. No description" \
		--opt-flags "'$(OPT_OPTS)'" \
		--model $*
ifneq ($(SEED),)
	@echo "$(SEED)" > $@/seed
endif
	@echo -e "[\033[0;34mtar\033[0m] generating tar file"
	@mkdir -p $@/package
	@cp $@/_module_/*.v $@/_module_/*.sv $@/metadata.toml $@/package
	@# Only copy the seed if we have one
	@[ -f $@/seed ] && cp $@/seed $@/package || true
	@tar cf $@/package_${@F}.tar.gz -C $@/package .




BUILD_DIRS=$(wildcard ${BUILD_DIR}/*/*)
PNR_FILES=$(patsubst ${BUILD_DIR}/%, ${BUILD_DIR}/%/synth/ecp5.config, ${BUILD_DIRS})
SYNTH_FILES=$(patsubst ${BUILD_DIR}/%, ${BUILD_DIR}/%/synth/ecp5.json, ${BUILD_DIRS})
HLSYNTH_FILES=$(patsubst ${BUILD_DIR}/%, ${BUILD_DIR}/%/hlsynth/synth.dot, ${BUILD_DIRS})

pnr: $(PNR_FILES) .PHONY
synth: $(SYNTH_FILES) .PHONY
hlsynth: $(HLSYNTH_FILES) .PHONY


# Yosys/nextpnr synthesis
${BUILD_DIR}/%/synth/ecp5.json: ${BUILD_DIR}/%/_module_/_module_.v Makefile .PRECIOUS
	@mkdir -p $(@D)
	@echo -e "[\033[0;34myosys\033[0m] synthing $@"
	@yosys \
		$$(ls $(<D)/*.v | grep -v interp ) $$(ls $(<D)/*.sv | grep -v interp | grep -v divider) \
		-p \
		"read_verilog -sv $$(ls $(<D)/_module_.v); \
		synth_ecp5 -json $@; \
		show \
		-prefix ${@D} \
		-colors 1; tee -o $(@D)/stat.txt stat"

# Set the seed if it exists
${BUILD_DIR}/%/synth/ecp5.config: SEEDFILE=$(patsubst ${BUILD_DIR}/%/synth/ecp5.config,${BUILD_DIR}/%/seed, $@)

${BUILD_DIR}/%/synth/ecp5.config: ${BUILD_DIR}/%/synth/ecp5.json ecpix5.lpf Makefile
	@mkdir -p $(@D)
	@echo -e "[\033[0;34mnextpnr\033[0m] PNR:ing $@"
	@nextpnr-ecp5 \
		--json $< \
		--textcfg $@ \
		--lpf ecpix5.lpf \
		--um5g-45k \
		--package CABGA554 \
		--lpf-allow-unconstrained \
		$(shell [ -f "$(SEEDFILE)" ] && printf -- "--seed=$$(cat $(SEEDFILE))") \
		2>&1 | tee $(@D)/pnr.log

${BUILD_DIR}/%/hlsynth/synth.dot: ${BUILD_DIR}/%/_module_/_module_.v Makefile
	@mkdir -p $(@D)
	@echo -e "[\033[0;34myosys\033[0m] synthing $@"
	@yosys -p "proc; opt; memory; opt; show -prefix ${@D}/synth -colors 1; tee -o $(@D)/stat.txt stat" $$(ls $(<D)/*.v)


clean: .PHONY
	rm -rf ${BUILD_DIR}

.PHONY:
.PRECIOUS: ${SYNTH_FILES}
