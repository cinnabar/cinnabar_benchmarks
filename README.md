# Public benchmarks for the Limestone/Cinnabar tool

Each benchmark resides in its own directory. The Makefile builds the cpp file
specified in the `BENCH_FILES` variable into corresponding directories inside
the `build` directory. It also builds a tar file containing all the generated
verilog files

The build directories are named as `<benchmark>_$(OPT_LEVEL)_$(WORD_LENGTH)` in
order to allow parallel stuff with varying word lengths and opt levels
