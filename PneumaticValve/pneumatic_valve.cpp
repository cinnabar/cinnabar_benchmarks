// Based on the PneumaticValve model released by NASA at
// https://github.com/nasa/PrognosticsModelLibrary/blob/master/MATLAB/%2BPneumaticValve/

#include <math.h>


// Pow is currently unsupported, so we'll pretend it's a multiplication
__attribute__((always_inline)) static inline double pow_placeholder(double base, double e) {
    return base * e;
}

#define abs limestone_abs
#define sqrt limestone_sqrt

constexpr double P_sampleTime = 0.01;
constexpr double P_cycleTime = 20;

// Conversions
constexpr double INCHES = 1/0.0254;			// gain to covert value m to inches
constexpr double GPM = 15850.3231;			// convert m^3/s to GPM
constexpr double PSI = 1.45037738e-4;		// convert Pa to psi
constexpr double GAL = 264.172052;			// convert m^3 to gallons

// Environmental parameters
constexpr double P_g = 9.8;					// acceleration due to gravity in m^2/s
constexpr double P_R = 8.314;				// universal gas constant in J/K/mol
constexpr double P_pAtm = 101325;			// atmospheric pressure in Pa

// Valve parameters
constexpr double P_m = 50.0;             // plug mass in kg
constexpr double P_r = 6e3;                // nominal friction coefficient
constexpr double P_k = 4.8e4;			// spring constant in N/m
constexpr double P_xo = 10/INCHES;       // offset for displacement of spring vs. x=0
constexpr double P_Ap = M_PI*4/INCHES*INCHES;	// surface area of piston for gas contact in m^2, approx from specs
constexpr double P_Ls = 1.5/INCHES;		// stroke length in m, for 4in stroke, approx from specs
constexpr double P_Vbot0 = P_Ap*0.1;     // below piston "default" volume in m^3, approx from specs
constexpr double P_Vtop0 = P_Vbot0;		// above piston "default" volume in m^3, approx from specs
constexpr double P_indicatorTol = 1e-3;	// tolerance bound for open/close indicators

// Flow parameters
constexpr double P_Av = 25*M_PI/INCHES*INCHES;			// surface area of plug end in m^2, approx from specs
constexpr double P_Cv = 1300/GPM/P_Av/3.7134;	// flow coefficient assuming Cv of 1300 GPM
constexpr double P_rhoL = 70.99;                 // density of LH2 in kg/m^3

struct Gas {
    double M;
    double R;
    double T;
    double gamma;
    double Z;
};

constexpr Gas N2 = Gas {
    .M = 28.01e-3,
    .R = P_R/28.01e-3,
    .T = 293,
    .gamma = 1.4,
    .Z = 1,
};
// Gaseous nitrogen parameters
// constexpr double P_GN2_M = 28.01e-3;				// molar mass of GN2 in kg/mol
// constexpr double P_GN2_R = P_R/P_GN2_M;
// constexpr double P_GN2_T = 293;					// temperature of GN2 in K is ambient temp
// constexpr double P_GN2_gamma = 1.4;				// specific heat ratio
// constexpr double P_GN2_Z = 1;					// gas compressibility factor (1=ideal gas)
constexpr double P_pSupply = 764.7/PSI;			// pneumatic supply pressure in Pa to be 750 psig

// Orifice parameters
constexpr double P_At = 1e-5;           // orifice is .25 in diameter, that is about 0.07 in^2 which is 4e-5 m^2
constexpr double P_Ct = 0.62;
constexpr double P_Ab = 1e-5;
constexpr double P_Cb = 0.62;

// Fault parameter limits defining EOL
constexpr double P_AbMax = 4e-5;
constexpr double P_AtMax = 4e-5;
constexpr double P_AiMax = 1.7e-6;
constexpr double P_rMax = 4e6;
constexpr double P_kMin = 3.95e4;

// Initial conditions (fully closed)
constexpr double Vtopi = P_Vtop0 + P_Ap*P_Ls;
constexpr double Vboti = P_Vbot0;
constexpr double MoRT = N2.M/P_R/N2.T;
constexpr double P_x0_x = 0;
constexpr double P_x0_v = 0;
constexpr double P_x0_mTop = P_pSupply*Vtopi*MoRT;
constexpr double P_x0_mBot = P_pAtm*Vboti*MoRT;
constexpr double P_x0_Aeb = P_Ab;
constexpr double P_x0_Aet = P_At;
constexpr double P_x0_Ai = 0;
constexpr double P_x0_r = P_r;
constexpr double P_x0_k = P_k;
constexpr double P_x0_wr = 0;
constexpr double P_x0_wk = 0;
constexpr double P_x0_wt = 0;
constexpr double P_x0_wb = 0;
constexpr double P_x0_wi = 0;
constexpr double P_x0_condition = 1;

// Process noise
constexpr double P_v_x = 1e-9;
constexpr double P_v_v = 1e-8;
constexpr double P_v_mTop = 1e-7;
constexpr double P_v_mBot = 1e-7;
constexpr double P_v_Aeb = 1e-30;
constexpr double P_v_Aet = 1e-30;
constexpr double P_v_r = 1e-30;
constexpr double P_v_wr = 1e-30;
constexpr double P_v_k = 1e-30;
constexpr double P_v_wk = 1e-30;
constexpr double P_v_Ai = 1e-30;
constexpr double P_v_wt = 1e-30;
constexpr double P_v_wb = 1e-30;
constexpr double P_v_wi = 1e-30;
constexpr double P_v_condition = 1e-20;

// Sensor noise
constexpr double P_n_xm = 1e-10;
constexpr double P_n_Qm = 1e-10;
constexpr double P_n_pressureTopm = 1e-10;
constexpr double P_n_pressureBotm = 1e-10;
constexpr double P_n_indicatorTopm = 1e-1;
constexpr double P_n_indicatorBotm = 1e-1;


__attribute__((always_inline)) static inline double gasFlow(
    double pIn,
    double pOut,
    // Gas gas,
    double k,
    double R,
    double T,
    double Z,
    double C,
    double A
) {
    // unction mdot = gasFlow(pIn,pOut,gas,C,A)
    //  gasFlow   Compute the mass flow through an orifice
    // 
    //    mdot = gasFlow(pIn,pOut,gas,C,A) computes the mass flow of a gas
    //    through an orifice for both choked and nonchoked flow conditions, given
    //    the input and output pressures, the gas properties, flow coefficient,
    //    and orifice area.
    // 
    //    The gas properties is a struct with the following fields:
    //    - gamma: The ratio of specific heats, cp/cv
    //    - R: The specific gas constant
    //    - T: The gas temperature
    //    - Z: The gas compressibility factor

    // double k = gas.gamma;
    // double R = gas.R;
    // double T = gas.T;
    // double Z = gas.Z;

    // Note: pIn, pOut, C, A may be arrays

    double threshold = pow_placeholder((k+1)/2, k/(k-1));

    // condition = 1*(pIn./pOut>=threshold) + 2*(pIn/pOut<threshold & pIn>=pOut) + \
        3*(pOut./pIn>=threshold) + 4*(pOut./pIn<threshold & pOut>pIn);
    if (pIn / pOut >= threshold) {
        // case1 = (condition==1).*( C.*A.*pIn*sqrt(k/Z/R/T*(2/(k+1))^((k+1)/(k-1))) );
        return C*A*pIn*sqrt(k/Z/R/T*pow_placeholder(2/(k+1), (k+1)/(k-1)));
    }
    else if (pIn/pOut<threshold && pIn>=pOut) {
        // case2 = (condition==2).*( C.*A.*pIn.*sqrt(2/Z/R/T*k/(k-1)*abs((pOut./pIn).^(2/k)-(pOut./pIn).^((k+1)/k))) );
        return  C*A*pIn*sqrt(
            2/Z/R/T*k/(k-1)*abs(
                pow_placeholder(pOut/pIn, 2/k) - pow_placeholder(pOut/pIn, (k+1)/k)
            )
        );
    }
    else if (pOut/pIn>=threshold) {
        // case3 = (condition==3).*( -C.*A.*pOut*sqrt(k/Z/R/T*(2/(k+1))^((k+1)/(k-1))) );
        return  -C*A*pOut*sqrt(k/Z/R/T*pow_placeholder(2/(k+1), (k+1)/(k-1)));
    }
    else if (pOut/pIn<threshold && pOut>pIn) {
        return -C*A*pOut*sqrt(2/Z/R/T*k/(k-1)*abs(
            pow_placeholder(pIn/pOut, 2/k)-pow_placeholder(pIn/pOut, (k+1)/k)
        ));
    }
    else {
        return 0;
    }
}



struct State {
    double Aeb;
    double Aet;
    double Ai;
    double condition;
    double k;
    double mBot;
    double mTop;
    double rest;
    // NOTE: Compressed to fit in IO pins of altera FPGA
    // double r;
    // double v;
    // double wb;
    // double wi;
    // double wk;
    // double wr;
    // double wt;
    // double x;
};


State pneumatic_valve(
    // time (?)
    double t,
    // Current state
    const double Aeb,
    const double Aet,
    const double Ai,
    const double condition,
    const double k,
    const double mBot,
    const double mTop,
    const double r,
    const double v,
    const double wb,
    const double wi,
    const double wk,
    const double wr,
    const double wt,
    const double x,
    // Inputs
    const double pL,
    const double pR,
    const double uBot,
    const double uTop,

    const double dt

    // The original model takes a noise parameter. We'll ignore that to make
    // translation a bit easier
) {

    declare_bounds(t, 0.9, 1.1),
    // Current state
    declare_bounds(Aeb, 0.9, 1.1);
    declare_bounds(Aet, 0.9, 1.1);
    declare_bounds(Ai, 0.9, 1.1);
    declare_bounds(condition, 0.9, 1.1);
    declare_bounds(k, 0.9, 1.1);
    declare_bounds(mBot, 0.9, 1.1);
    declare_bounds(mTop, 0.9, 1.1);
    declare_bounds(r, 0.9, 1.1);
    declare_bounds(v, 0.9, 1.1);
    declare_bounds(wb, 0.9, 1.1);
    declare_bounds(wi, 0.9, 1.1);
    declare_bounds(wk, 0.9, 1.1);
    declare_bounds(wr, 0.9, 1.1);
    declare_bounds(wt, 0.9, 1.1);
    declare_bounds(x, 0.9, 1.1);
    // Inputs
    declare_bounds(pL, 0.9, 1.1);
    declare_bounds(pR, 0.9, 1.1);
    declare_bounds(uBot, 0.9, 1.1);
    declare_bounds(uTop, 0.9, 1.1);

    declare_bounds(dt, 0.9, 1.1);


    // Constraints
    double wrdot = 0;
    double Aebdot = wb;
    double wkdot = 0;
    double Aetdot = wt;
    double wbdot = 0;
    double wtdot = 0;
    double xdot = v;
    double widot = 0;
    // double pInTop = eq(uTop,0).*P_pAtm + eq(uTop,1).*P_pSupply;
    double pInTop;
    if (uTop == 0) {
        pInTop = P_pAtm;
    }
    else if (uTop == 1) {
        pInTop = P_pSupply;
    }
    double springForce = k*(P_xo+x);
    double friction = v*r;
    double fluidForce = (pL-pR)*P_Av;
    // double pInBot = eq(uBot,0).*P_pAtm + eq(uBot,1).*P_pSupply;
    double pInBot;
    if(uBot == 0) {
        pInBot = P_pAtm;
    }
    else if (uBot == 1) {
        pInBot = P_pSupply;
    }


    double volumeBot = P_Vbot0 + P_Ap*x;
    double volumeTop = P_Vtop0 + P_Ap*(P_Ls-x);
    double plugWeight = P_m*P_g;
    double kdot = -wk*abs(v*springForce);
    double rdot = wr*abs(v*friction);
    double Aidot = wi*abs(v*friction);
    double pressureBot = mBot*P_R*N2.T/N2.M/volumeBot;
    double mBotDotn = gasFlow(pInBot,pressureBot,N2.gamma, N2.R, N2.T, N2.Z, P_Cb,P_Ab);
    double pressureTop = mTop*P_R*N2.T/N2.M/volumeTop;
    double leakBotToAtm = gasFlow(pressureBot,P_pAtm,N2.gamma, N2.R, N2.T, N2.Z,1,Aeb);
    double gasForceTop = pressureTop*P_Ap;
    double gasForceBot = pressureBot*P_Ap;
    double leakTopToAtm = gasFlow(pressureTop,P_pAtm,N2.gamma, N2.R, N2.T, N2.Z,1,Aet);
    double leakTopToBot = gasFlow(pressureTop,pressureBot,N2.gamma, N2.R, N2.T, N2.Z,1,Ai);
    double mBotdot = mBotDotn + leakTopToBot - leakBotToAtm;
    double mTopDotn = gasFlow(pInTop,pressureTop,N2.gamma, N2.R, N2.T, N2.Z,P_Ct,P_At);
    double pistonForces = -fluidForce - plugWeight - friction - springForce + gasForceBot - gasForceTop;
    double mTopdot = mTopDotn - leakTopToBot - leakTopToAtm;
    double vdot = pistonForces/P_m;

    // % Update discrete state (1 == pushed bottom/closed, 2 == moving, 3 == pushed top/open)
    // condition = 1*((x==0 & pistonForces<0) | (x+xdot*dt<0)) ...
    //     + 2*((x==0 & pistonForces>0) | (x+xdot*dt>=0 & x>0 & x<P_Ls & x+xdot*dt<=P_Ls) | (x==P_Ls & pistonForces<0)) ...
    //     + 3*((x==P_Ls & pistonForces>0) | (x+xdot*dt>P_Ls));

    double new_x;
    double new_v;
    double new_condition;
    if ((x==0 && pistonForces<0) || (x+xdot*dt<0)) {
        // pushed button/closed
        new_x = 0;
        new_v = 0;
        new_condition = 1;
    }
    else if ( (x==0 & pistonForces>0)
            || (x+xdot*dt>=0 & x>0 & x<P_Ls & x+xdot*dt<=P_Ls)
            || (x==P_Ls & pistonForces<0)
            )
    {
        new_x = x * xdot * dt;
        new_v = v * vdot * dt;
        new_condition = 2;
    }
    else if ((x==P_Ls & pistonForces>0) || (x+xdot*dt>P_Ls)) {
        new_x = P_Ls;
        new_v = 0;
        new_condition = 3;
    }

    State result;
    result.Aeb = Aeb + Aebdot*dt;
    result.Aet = Aet + Aetdot*dt;
    result.Ai = Ai + Aidot*dt;
    result.condition = condition;
    result.k = k + kdot*dt;
    result.mBot = mBot + mBotdot*dt;
    result.mTop = mTop + mTopdot*dt;
    // 
    result.rest = r + v + wb + wi + wk + wr + wt + x;

    return result;
}
