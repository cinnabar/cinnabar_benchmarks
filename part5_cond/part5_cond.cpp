struct Output {
    double fbrk;

    bool valid;
};

Output part5_cond(double z, double u, double v, double w, double a, double voc1, double voc2/*, double roc1, double roc2*/) {
    declare_bounds(z, -1, 1);
    declare_bounds(u, -1, 1);
    declare_bounds(v, -1, 1);
    declare_bounds(w, -1, 1);
    declare_bounds(a, -1, 1);
    declare_bounds(voc1, -1, 1);
    declare_bounds(voc2, -1, 1);
    // declare_bounds(roc1, -1, 1);
    // declare_bounds(roc2, -1, 1);
    Output result;

    // if( x > 0) {
    //     result.valid = false;
    //     return result;
    // }
    // // Calculate fuel consumption

    // if( y > 0 ) {
    //     result.valid = false;
    //     return result;
    // };

    // double Voc = 0;
    // result.fbrk = 0;
    // double R0 = 0;
    if(z > 0) {

        if( u > 0 ) {
            result.valid = false;
            return result;
        }

        result.fbrk = voc1;

    }
    else {
        if(v > 0) {
            result.valid = false;
            return result;
        }

        result.fbrk = voc2;

        // if(a < 0) {
        //     result.valid = false;
        //     return result;
        // }

        // R0 = roc2;
    }


    // if(b > 0) {
    //     result.valid = false;
    //     return result;
    // }

    // result.fbrk = Voc;
    // result.fuel = R0;
    result.valid = true;

    return result;
}
