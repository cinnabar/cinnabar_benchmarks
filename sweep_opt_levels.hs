#!/usr/bin/env stack
{- stack script
 --resolver lts-14.18
 --package "string-interpolate"
 --package "text"
 --package "containers"
 --package "process"
 --package "ghc"
 --package "hashmap"
 --ghc-options -Wall
-}

{-# language OverloadedStrings #-}
{-# language QuasiQuotes #-}
{-# language BinaryLiterals #-}

import qualified Data.HashMap as HashMap ((!), Map, fromList, keys)
import Data.String.Interpolate (i)
import qualified Data.Text as T
import qualified Data.Text.IO as IO
import qualified System.Process as Process
import System.Exit (ExitCode (ExitSuccess), exitFailure)
import System.Environment (getArgs)

data Args = Args
    { name :: T.Text
    , wl :: T.Text
    , opt :: Maybe T.Text
    , extraArgs :: [T.Text]
    , seed :: Maybe Int
    }

-- wl = 28
defaultWordLength :: T.Text
-- defaultWordLength = "FixedTotalLength_31"
-- defaultWordLength = "FullySpecified_24_7"
defaultWordLength = "FullySpecified_23_24"

baseArgs :: [Args]
baseArgs = 
    [ Args "masksnot"       defaultWordLength (Just "O1") ["--compress-inputs", "--no-retiming", "-pRemoveSingleSelect", "--phi-masks"] Nothing
    , Args "nocachenot"     defaultWordLength (Just "O1") ["--compress-inputs", "--no-retiming", "-pRemoveSingleSelect", "--no-sig-path-cache"] Nothing
    , Args "cachenot"       defaultWordLength (Just "O1") ["--compress-inputs", "--no-retiming", "-pRemoveSingleSelect"] Nothing
    ]

addPartArg :: Int -> Args -> Args
addPartArg part args =
    Args
        ((name args) `T.append` [i|p#{part}|])
        (wl args)
        (opt args)
        ((extraArgs args) ++ [[i|-C-DPART#{part} -C-DSEQUENTIAL_PARTS|]])
        (seed args)

addCliArg :: (T.Text, T.Text) -> Args -> Args
addCliArg (filenameChunk, cliArg) args =
    Args
        ((name args) `T.append` [i|#{filenameChunk}|])
        (wl args)
        (opt args)
        ((extraArgs args) ++ [[i|#{cliArg}|]])
        (seed args)

addOptPass :: (T.Text, T.Text) -> Args -> Args
addOptPass (filenameChunk, passName) args =
    addCliArg (filenameChunk, [i|"-p#{passName}"|]) args
    -- Args
    --     ((name args) `T.append` [i|o#{filenameChunk}|])
    --     (wl args)
    --     (opt args)
    --     ((extraArgs args) ++ [[i|-p#{passName}|]])
    --     (seed args)

addOptPasses :: [(T.Text, T.Text)] -> Args -> Args
addOptPasses passes args =
    foldr addOptPass args passes

addCliArgs :: [(T.Text, T.Text)] -> Args -> Args
addCliArgs cliArgs args =
    foldr addCliArg args cliArgs

partedArgs :: [Int] -> [Args]
partedArgs parts =
    concat $ fmap (\part -> fmap (addPartArg part) baseArgs) parts

setSeedArg :: Int -> Args -> Args
setSeedArg newSeed args =
    Args
        ((name args) `T.append` [i|s#{newSeed}|])
        (wl args)
        (opt args)
        (extraArgs args)
        (Just newSeed)


setWordLength :: T.Text -> Args -> Args
setWordLength newWl args =
    Args
        ((name args) `T.append` [i|w#{newWl}|])
        (newWl)
        (opt args)
        (extraArgs args)
        (seed args)

part5SeedArgs :: [Int] -> [Args]
part5SeedArgs seeds =
    concat $ fmap (\newSeed -> fmap (setSeedArg newSeed) (partedArgs [5])) seeds


part5WordLengths :: [T.Text] -> [Args]
part5WordLengths lengths =
    concat $ fmap (\newWl -> fmap (setWordLength newWl) (partedArgs [5])) lengths

partedElseArgs :: [[(T.Text, T.Text)]] -> [Args]
partedElseArgs passes =
    concat $ fmap (\pass -> fmap (addOptPasses pass) (partedArgs [5])) passes

part5WithArgs :: [[(T.Text, T.Text)]] -> [Args]
part5WithArgs argBundles =
    concat $ fmap (\argBundle -> fmap (addCliArgs argBundle) (partedArgs [5])) argBundles

partedWithArgs :: [[(T.Text, T.Text)]] -> [Args]
partedWithArgs argBundles =
    concat $ fmap (\argBundle -> fmap (addCliArgs argBundle) (partedArgs [1..7])) argBundles

-- args =
--     -- [ Args ""       28 (Just "O0") ["--compress-inputs --no-slp-vectorization"]
--     [ Args "masks"       28 (Just "O1") ["--compress-inputs --phi-masks"]
--     , Args "nocache"       28 (Just "O1") ["--compress-inputs --no-sig-path-cache"]
--     , Args "cache"       28 (Just "O1") ["--compress-inputs"]
--     -- , Args "masks"       28 (Just "O1") ["--phi-masks --compress-inputs"]
--     -- , Args ""       28 (Just "O2") ["--compress-inputs --no-slp-vectorization"]
--     -- , Args ""       28 (Just "O3") ["--compress-inputs --no-slp-vectorization"]
--     -- , Args "ufp"    28 (Just "O0") ["--compress-inputs --no-slp-vectorization", "--unsafe-fp"]
--     -- , Args "ufp"    28 (Just "O1") ["--compress-inputs --no-slp-vectorization", "--unsafe-fp"]
--     -- , Args "ufp"    28 (Just "O2") ["--compress-inputs --no-slp-vectorization", "--unsafe-fp"]
--     -- , Args "ufp"    28 (Just "O3") ["--compress-inputs --no-slp-vectorization", "--unsafe-fp"]
--     -- , Args "dce"    28 (Just "O0") ["--compress-inputs --no-slp-vectorization", "--dce"]
--     -- , Args "dce"    28 (Just "O1") ["--compress-inputs --no-slp-vectorization", "--dce"]
--     -- , Args "dce"    28 (Just "O2") ["--compress-inputs --no-slp-vectorization", "--dce"]
--     -- , Args "dce"    28 (Just "O3") ["--compress-inputs --no-slp-vectorization", "--dce"]
--     -- , Args "edce"    28 (Just "O0") ["--compress-inputs --no-slp-vectorization", "--edce"]
--     -- , Args "edce"    28 (Just "O1") ["--compress-inputs --no-slp-vectorization", "--edce"]
--     -- , Args "edce"    28 (Just "O2") ["--compress-inputs --no-slp-vectorization", "--edce"]
--     -- , Args "edce"    28 (Just "O3") ["--compress-inputs --no-slp-vectorization", "--edce"]
--     ]


argToMake :: Args -> T.Text
argToMake args =
    let
        eargs = T.intercalate " " $ extraArgs args
        optArg :: T.Text
        optArg =
            case opt args of
                Just opts -> [i|OPT=#{opts}|]
                Nothing -> ""

        seedArg :: T.Text
        seedArg =
            case seed args of
                Just s -> [i|SEED=#{s}|]
                Nothing -> ""
    in
        [i|make DESCRIPTION=#{name args} #{optArg} WL=#{wl args} OPT_OPTS='#{eargs}' #{seedArg}|]


runCommands :: [T.Text] -> IO ()
runCommands commands = do
    handles <- sequence $ fmap Process.spawnCommand $ fmap T.unpack commands
    exitCodes <- sequence $ fmap Process.waitForProcess $ handles
    let failed = filter (\(_, code) -> code /= ExitSuccess) $ zip commands exitCodes
    IO.putStrLn "The following commands failed:"
    sequence_ $ fmap IO.putStrLn $ fmap fst failed



zeroArgHandler :: [Args] -> [T.Text] -> IO [Args]
zeroArgHandler args [] = pure args
zeroArgHandler _ got = do
    putStrLn [i|expected 0 additional arguments, got #{got}|]
    exitFailure

-- Top level CLI argument parsing
sweepKinds :: HashMap.Map T.Text ([T.Text] -> IO [Args])
sweepKinds = HashMap.fromList
    [ ("parted", zeroArgHandler $ partedArgs [1..7])
    , ("part5_seed", zeroArgHandler $ part5SeedArgs [1..7])
    , ("part5_flat", zeroArgHandler $ part5SeedArgs [1])
    , ("part5_else", zeroArgHandler $ part5WithArgs [[], [("l", "SelectElseLast")], [("x", "SelectElseX")]])
    , ("part5_filter_undef", zeroArgHandler $ part5WithArgs
        [ []
        , [("f", "--filter-undef")]
        , [("f", "--filter-undef"), ("x", "-pSelectElseX")]
        , [("f", "--filter-undef"), ("l", "-pSelectElseLast")]
        ]
      )
    , ("part5_filter_undef_with_naive", zeroArgHandler $ part5WithArgs
        [ []
        , [("f", "--filter-undef")]
        , [("f", "--filter-undef"), ("x", "-pSelectElseX")]
        , [("f", "--filter-undef"), ("l", "-pSelectElseLast")]
        , [("n", "--naive-paths")]
        , [("n", "--naive-paths"), ("f", "--filter-undef")]
        , [("n", "--naive-paths"), ("f", "--filter-undef"), ("x", "-pSelectElseX")]
        , [("n", "--naive-paths"), ("f", "--filter-undef"), ("l", "-pSelectElseLast")]
        ]
      )
    , ("parted_filter_undef", zeroArgHandler $ partedWithArgs
        [ []
        , [("f", "--filter-undef")]
        , [("f", "--filter-undef"), ("x", "-pSelectElseX")]
        , [("f", "--filter-undef"), ("l", "-pSelectElseLast")]
        ]
      )
    ]

main :: IO ()
main = do
    args <- getArgs
    let argsT = fmap T.pack args
    makeArgs <- case argsT of
        (arg:rest) ->
            pure $ (HashMap.!) sweepKinds arg
        [] -> do
            putStrLn [i|Sweep type expected (#{HashMap.keys sweepKinds})|]
            exitFailure
    actualArgs <- makeArgs $ tail argsT
    Process.callCommand "make clean"
    runCommands $ fmap argToMake actualArgs
    -- putStrLn $ show $ fmap T.unpack $ fmap argToMake actualArgs

