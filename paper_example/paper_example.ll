
; ModuleID = 'simple.cpp'
source_filename = "simple.cpp"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

declare dso_local void @declare_bounds(double, double, double) #2

; Function Attrs: norecurse nounwind readnone sspstrong uwtable
define dso_local double @_Z6simplef(
  double %x,
  double %cond1_,
  double %cond2_,
  double %cond3_,
  double %cond4_
) local_unnamed_addr #0 {
0:
  br label %1
1:
  call void @declare_bounds(double %x, double -10.0, double 10.0)
  call void @declare_bounds(double %cond1_, double -10.0, double 10.0)
  call void @declare_bounds(double %cond2_, double -10.0, double 10.0)
  call void @declare_bounds(double %cond3_, double -10.0, double 10.0)
  call void @declare_bounds(double %cond4_, double -10.0, double 10.0)

  %cond1 = fcmp ogt double %cond1_, 1.0
  br i1 %cond1, label %2, label %8

2:
  %cond2 = fcmp ogt double %cond2_, 1.0
  br i1 %cond2, label %8, label %3

3:
  %a = fadd double %x, 1.0
  %cond3 = fcmp ogt double %cond3_, 1.0
  br i1 %cond3, label %4, label %5

4:
  %y1 = fmul double %a, %x
  br label %6

5:
  %y2 = fadd double %a, %x
  br label %6

6:
  %y = phi double [%y1, %4], [%y2, %5]
  %z = fmul double %y, %y
  %cond4 = fcmp ogt double %cond4_, 1.0
  br i1 %cond4, label %8, label %7

7:
  br label %9

8:
  %result = phi double [0.0, %1], [%x, %2], [%z, %6]
  br label %9

9:
  %_output = phi double [%result, %8], [undef, %7]
  ret double %_output
}

