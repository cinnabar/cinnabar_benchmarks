//#include "omp.h"
#include "map.h"

// #include <liblimestone.hpp>

// All defined constants

#ifndef __IS_CINNABAR__
#include <math.h>

#define limestone_sqrt sqrt
#define limestone_max fmax
#define limestone_min fmin
#define limestone_abs fabs
#define limestone_trunc (int)


void __invisible_nop(double) {}

#include "../hev_model/interpolation_impl.hpp"

extern "C" {
    __attribute__((optnone)) void __gdb_trampoline() {}
}
#else
#define __gdb_trampoline()
#endif

#define MYINF                   10000

// Model PARAMETERS
#define R_WHEEL                 0.30
#define FINAL_DRIVE             3.4
#define MASS                    1500
#define TRANS_BSG               2.5127

#define W_ENG_MAX               605

#define CAPACITY_CELL           30000
#define CURR_BSG_MAX            50
#define CURR_BSG_MIN            -200
#define P_BSG_MAX               13000
#define P_BSG_MIN               -17000

#define Q_LHV                   50000
#define A                       40
#define B_MS                    0.5
#define C_MS                    0.3

#define SOC0                    0.60


// Global parameters

// Add ability to set GAMMA as input

#ifndef USE_ORIGINAL_MAPS
#include "../hev_model/maps.hpp"
#else
#include "../hev_model/maps_original.hpp"
#endif

#ifdef PART8
#define ONLY_PART8
#endif
#ifdef PART7
#define ONLY_PART7
#endif
#ifdef PART6
#define ONLY_PART6
#endif
#ifdef PART5
#define ONLY_PART5
#endif
#ifdef PART4
#define ONLY_PART4
#endif
#ifdef PART3
#define ONLY_PART3
#endif
#ifdef PART2
#define ONLY_PART2
#endif
#ifdef PART1
#define ONLY_PART1
#endif

#ifdef SEQUENTIAL_PARTS
#ifdef PART8
#define PART7
#endif
#ifdef PART7
#define PART6
#endif
#ifdef PART6
#define PART5
#endif
#ifdef PART5
#define PART4
#endif
#ifdef PART4
#define PART3
#endif
#ifdef PART3
#define PART2
#endif
#ifdef PART2
#define PART1
#endif
#endif

// NOTE: I'm no longer quite sure what opaque barrier does, but for the output
// mercverf tests we'll define it away
#define opaque_barrier(VAR) VAR

#if defined(PART1)
#define OPAQUE_PART1(NEW_VAR, VAR) double NEW_VAR = opaque_barrier(VAR)
#else
#define OPAQUE_PART1(NEW_VAR, VAR) NEW_VAR = VAR
#endif

#if defined(PART2)
#define OPAQUE_PART2(NEW_VAR, VAR) double NEW_VAR = opaque_barrier(VAR)
#else
#define OPAQUE_PART2(NEW_VAR, VAR) NEW_VAR = VAR
#endif

#if defined(PART3)
#define OPAQUE_PART3(NEW_VAR, VAR) double NEW_VAR = opaque_barrier(VAR)
#else
#define OPAQUE_PART3(NEW_VAR, VAR) NEW_VAR = VAR
#endif

#if defined(PART4)
#define OPAQUE_PART4(NEW_VAR, VAR) double NEW_VAR = opaque_barrier(VAR)
#else
#define OPAQUE_PART4(NEW_VAR, VAR) NEW_VAR = VAR
#endif

#if defined(PART5)
#define OPAQUE_PART5(NEW_VAR, VAR) double NEW_VAR = opaque_barrier(VAR)
#else
#define OPAQUE_PART5(NEW_VAR, VAR) NEW_VAR = VAR
#endif

#if defined(PART6)
#define OPAQUE_PART6(NEW_VAR, VAR) double NEW_VAR = opaque_barrier(VAR)
#else
#define OPAQUE_PART6(NEW_VAR, VAR) NEW_VAR = VAR
#endif

#if defined(PART7)
#define OPAQUE_PART7(NEW_VAR, VAR) double NEW_VAR = opaque_barrier(VAR)
#else
#define OPAQUE_PART7(NEW_VAR, VAR) NEW_VAR = VAR
#endif

#ifndef HARDINTERP
#include "../hev_model/soft_interp.hpp"
#endif


/*
double inline __attribute__((always_inline)) interp2equid(double const*, double const*, double const*, double x, double y) {
    return x+y;
}

double inline __attribute__((always_inline)) interp1equid(double const* rad_s_Trq_bsg_min, double const* Trq_bsg_min, double x) {
    return x+x;
}
*/


struct Output {
    double stage_cost;
    double x1next;
    double x2next;
    double fuel;
    double time;
    double teng;
    double fbrk;

    bool valid;
};

#define DOUBLE_ARG(NAME) double NAME


#define DUMMY_OUTPUT(VAL) \
    result.stage_cost = VAL; \
    result.x1next = VAL; \
    result.x2next = VAL; \
    result.fuel = VAL; \
    result.time = VAL; \
    result.teng = VAL; \
    result.fbrk = x1+x2+u1+u2+u3+dt; \
    result.valid = true; \

#define DUMMY_OUTPUT2(VAL, VAL2) \
    result.stage_cost = VAL; \
    result.x1next = VAL2; \
    result.x2next = VAL; \
    result.fuel = VAL; \
    result.time = VAL; \
    result.teng = VAL; \
    result.fbrk = x1+x2+u1+u2+u3+dt; \
    result.valid = true; \

#define DUMMY_OUTPUT3(VAL, VAL2, VAL3) \
    result.stage_cost = VAL; \
    result.x1next = VAL2; \
    result.x2next = VAL3; \
    result.fuel = VAL; \
    result.time = VAL; \
    result.teng = VAL; \
    result.fbrk = x1+x2+u1+u2+u3+dt; \
    result.valid = true; \

#define DUMMY_OUTPUT4(VAL, VAL2, VAL3, VAL4) \
    result.stage_cost = VAL; \
    result.x1next = VAL2; \
    result.x2next = VAL3; \
    result.fuel = VAL4; \
    result.time = VAL; \
    result.teng = VAL; \
    result.fbrk = x1+x2+u1+u2+u3+dt; \
    result.valid = true; \

#define DUMMY_OUTPUT5(VAL, VAL2, VAL3, VAL4, VAL5) \
    result.stage_cost = VAL; \
    result.x1next = VAL2; \
    result.x2next = VAL3; \
    result.fuel = VAL4; \
    result.time = VAL5; \
    result.teng = VAL; \
    result.fbrk = x1+x2+u1+u2+u3+dt; \
    result.valid = true; \

#define DUMMY_OUTPUT6(VAL, VAL2, VAL3, VAL4, VAL5, VAL6) \
    result.stage_cost = VAL; \
    result.x1next = VAL2; \
    result.x2next = VAL3; \
    result.fuel = VAL4; \
    result.time = VAL5; \
    result.teng = VAL6; \
    result.fbrk = x1+x2+u1+u2+u3+dt; \
    result.valid = true; \

#define DUMMY_OUTPUT7(VAL, VAL2, VAL3, VAL4, VAL5, VAL6, VAL7) \
    result.stage_cost = VAL; \
    result.x1next = VAL2; \
    result.x2next = VAL3; \
    result.fuel = VAL4; \
    result.time = VAL5; \
    result.teng = VAL6; \
    result.fbrk = VAL7 + x1+x2+u1+u2+u3+dt; \
    result.valid = true; \
// Functions
Output reference_pt_model(
    double x1,
    double x2,
    double u1,
    double u2,
    // NOTE: This used to be an int
    double u3,
    double dt
) {
    // TODO: Set correct bounds
#ifdef __IS_CINNABAR__
    declare_bounds(x1, 0, 1156);
    declare_bounds(x2, 0.3, 0.8);
    declare_bounds(u1, -360, 360);
    declare_bounds(u2, -42, 42);
    declare_bounds(u3, 1, 6);
    declare_bounds(dt, 10, 10);
#endif

    // TODO: Set gamma
#ifdef PART7
    double _GAMMA = 0.3;// = 0.0001;
#endif



    // double* rad_s_Trq_bsg_max = limestone_declare_lut({});

    Output result;
#ifdef PART1
    
    // TODO: Add intrinsic
    // double v1 = limestone_sqrt(x1);
    double v1 = limestone_sqrt(x1);
    
    // TODO: Add intrinsic
    if( limestone_abs(v1) < 1e-9 ) {
        v1 = 0;
    }
    
    // Wheel speed (rad/s)
    double w_wheel = v1/R_WHEEL;
    
    // Rotational speed at transmission output shaft
    double w_trans_o = w_wheel*FINAL_DRIVE;
    
    // Select gear
    double gear = u3;
    
    // TODO: Check if trunc does what we expect here
    double gear_ratio = gear_ratios[ limestone_trunc(gear-1) ];
    // gear_ratio = limestone_
        
    // Calculate engine speed
    double w_eng = limestone_max(w_trans_o*gear_ratio, 0);
    #if 0
        if( T_pt_tmp < 0 ) {
            w_eng = std::max<double>(w_trans_o*gear_ratio, 0);
        }
        else {
            w_eng = w_trans_o*gear_ratio;
        }
    #endif

    // If engine speed is too high
#ifdef ONLY_PART1
    if(w_eng > W_ENG_MAX) {
        result.valid = false;
        return result;
    }
#endif
    w_eng = limestone_min(w_eng, W_ENG_MAX); // NOTE: Remove when infering bounds is automatic

#ifndef PART2
    DUMMY_OUTPUT(w_eng, x1 + x2 + u1 + u2 + u3 + dt)
    return result;
#endif

#endif
#ifdef PART2
    OPAQUE_PART1(o2_w_eng, w_eng);

    
     // Calculate BSG speed
     double w_bsg = TRANS_BSG*o2_w_eng;
 
     // BSG torque
     double T_bsg = u2;
    
     // Calculate max and min bsg torque
#ifdef ONLY_PART2
     if( !interp1equid_valid(rad_s_Trq_bsg_min, Trq_bsg_min, w_bsg)) {
         result.valid = false;
         return result;
     }
#endif
    double T_bsg_min = interp1equid(rad_s_Trq_bsg_min, Trq_bsg_min, w_bsg);

#ifdef ONLY_PART2
    if( !interp1equid_valid(rad_s_Trq_bsg_max, Trq_bsg_max, w_bsg)) {
        result.valid = false;
        return result;
    }
#endif
    double T_bsg_max = interp1equid(rad_s_Trq_bsg_max, Trq_bsg_max, w_bsg);

    
    // Check if T_bsg lies outside of valid range
#ifdef ONLY_PART2
    if(T_bsg > T_bsg_max) {
        result.valid = false;
        return result;
    }
#endif
    T_bsg = limestone_min(T_bsg, T_bsg_max); // NOTE: Remove when infering bounds is automatic
   
#ifdef ONLY_PART2
    if(T_bsg < T_bsg_min) {
        result.valid = false;
        return result;
    }
#endif
    T_bsg = limestone_max(T_bsg, T_bsg_min); // NOTE: Remove when infering bounds is automatic


#ifndef PART3
    DUMMY_OUTPUT6(T_bsg, w_eng, T_bsg_min, T_bsg_max, gear_ratio, v1);
    return result;
#endif

#endif
#ifdef PART3
    OPAQUE_PART3(o3_w_eng, w_eng);
    OPAQUE_PART3(o_T_bsg_min, T_bsg_min);
    OPAQUE_PART3(o_T_bsg_max, T_bsg_max);
    OPAQUE_PART3(o3_gear_ratio, gear_ratio);
    OPAQUE_PART3(o3_v1, v1);

    // Calculate torque limits
#ifdef ONLY_PART3
    if( !interp1equid_valid(eng_spd_curve_max_torqe, max_torque_curve, o3_w_eng) ) {
        result.valid = false;
        return result;
    }
#endif
    double T_eng_max = interp1equid(eng_spd_curve_max_torqe, max_torque_curve, o3_w_eng);

    // Powertrain torque is limited by what can be provided by engine and BSG motor
    double T_pt_min = o_T_bsg_min*TRANS_BSG;
    double T_pt_max = o_T_bsg_max*TRANS_BSG + T_eng_max;


    double T_pt = limestone_max(u1, T_pt_min);
    T_pt = limestone_min(T_pt, T_pt_max);

    // Calculate T_out at wheel
    double T_out_ideal = FINAL_DRIVE*o3_gear_ratio*T_pt;

    double eff_trans_matrix = 0.99;
    double T_out;
    if( T_out_ideal >= 0)
        T_out = T_out_ideal*eff_trans_matrix;
    else
        T_out = T_out_ideal/eff_trans_matrix;

    // Determine brake force
    // TODO: Add intrinsic
    double F_brk = limestone_max( -(u1 - T_pt)*375/15.6, 0); //-(u1 - T_pt)*375/24

    // We cannot brake more than down to 0 m/s
    // TODO: Add intrinsic
    double Flb = limestone_max(x1*(MASS/(2*dt) )+T_out/R_WHEEL - A - B_MS*o3_v1 -C_MS*x1, 0);

    // Brake force cannot result in negative velocity
    double Fbrk_sat = limestone_min(F_brk,Flb);
    Fbrk_sat = limestone_min(Fbrk_sat,3000);
    Fbrk_sat = limestone_max(Fbrk_sat, 0);

#ifndef PART4
    DUMMY_OUTPUT5(T_out, Fbrk_sat, v1, gear_ratio, w_eng)
    return result;
#endif

#endif
#ifdef PART4
    OPAQUE_PART4(o_T_out, T_out);
    OPAQUE_PART4(o_Fbrk_sat, Fbrk_sat);
    OPAQUE_PART4(o_v1, v1);
    OPAQUE_PART4(o_gear_ratio, gear_ratio);
    OPAQUE_PART4(o_w_eng, w_eng);

    // Update velocity
    double v2 = x1 + ( 2*dt/MASS )*(o_T_out/R_WHEEL - o_Fbrk_sat - A - B_MS*o_v1 -C_MS*x1 );

    
    // T_out is a too large negative number resulting in negative speed
#ifdef ONLY_PART4
    if( v2 < -1e-9 ) {
        result.valid = false;
        return result;
    }
#endif
    v2 = limestone_max(v2, 0 ); // NOTE: Remove when infering bounds is automatic

    // Compute update speed
    
    // To avoid numerical issues with insanely (!) small numbers
    double v = limestone_sqrt( v2 );

    // Compute final speed at interval
    double w_wheel_p = v/R_WHEEL;
    double w_eng_p = w_wheel_p*FINAL_DRIVE*o_gear_ratio;

#ifdef ONLY_PART4
    if( w_eng_p > W_ENG_MAX) {
        result.valid = false;
        return result;
    }
#endif
    w_eng_p = limestone_min(w_eng_p, W_ENG_MAX); // NOTE: Remove when infering bounds is automatic

    // Calculate average speed
    double w_eng_avg = ( o_w_eng + w_eng_p )/2;
    double v_veh_avg = ( limestone_sqrt(x1) + v )/2;

    // Ignore really slow average speeds
#ifdef ONLY_PART4
    if(v_veh_avg < 0.01) {
        result.valid = false;
        return result;
    }
#endif
    v_veh_avg = limestone_max(v_veh_avg, 0.01); // NOTE: Remove when infering bounds is automatic

#ifndef PART5
    DUMMY_OUTPUT5(v_veh_avg, w_eng_avg, T_pt, T_bsg, T_eng_max)
    return result;
#endif

#endif
#ifdef PART5
    OPAQUE_PART5(o_w_eng_avg, w_eng_avg);
    OPAQUE_PART5(o_T_pt, T_pt);
    OPAQUE_PART5(o5_T_bsg, T_bsg);
    OPAQUE_PART5(o_T_eng_max, T_eng_max);

    // BSG average speed
    double w_bsg_avg = TRANS_BSG*o_w_eng_avg;

    // Compute remaining torque to be provided by engine
    double T_eng = o_T_pt - TRANS_BSG*o5_T_bsg;
    
#ifdef ONLY_PART5
    if( T_eng > o_T_eng_max) {
        result.valid = false;
        return result;
    }
#endif
    T_eng = limestone_min(T_eng, o_T_eng_max); // NOTE: Remove when infering bounds is automatic
    T_eng = limestone_max(T_eng, 0);

    // Calculate fuel consumption
#ifdef ONLY_PART5
    if( !interp2equid_valid(we_list_fuel_rad_s, T_list_fuel_Nm, fuel_map, o_w_eng_avg, T_eng) ) {
        result.valid = false;
        return result;
    };
#endif
    double m_fuel = interp2equid(we_list_fuel_rad_s, T_list_fuel_Nm, fuel_map, o_w_eng_avg, T_eng);

    double Voc = 0;
    double R0 = 0;
    if(o5_T_bsg <= 0) {
#ifdef ONLY_PART5
        if( !interp1equid_valid(SOC_table_Voc, Voc_table_ch, x2) ) {
            result.valid = false;
            return result;
        }
#endif
        Voc = interp1equid(SOC_table_Voc, Voc_table_ch, x2);
#ifdef ONLY_PART5
        if( !interp1equid_valid(SOC_table_R0, R0_table_ch, x2) ) {
            result.valid = false;
            return result;
        }
#endif
        R0 = interp1equid(SOC_table_R0, R0_table_ch, x2);
        // NOTE: Without this here, LLVM figures out that R0_table_ch and
        // R0_table_dc are just pointers, and converts the code into
        //
        // table_ptr = cond ? R0_table_ch : R0_table_dc R0 =
        // interp1equid(SOC_table_R0, table_ptr, x2).
        //
        // Since the poitner is no longer global, limestone freaks out. By
        // doing a nop on the result in this branch, we prevent this (cool but)
        // useless beiavhour
        __invisible_nop(R0);
    }
    else {
#ifdef ONLY_PART5
        if( !interp1equid_valid(SOC_table_Voc, Voc_table_dc, x2) ) {
            result.valid = false;
            return result;
        }
#endif
        Voc = interp1equid(SOC_table_Voc, Voc_table_dc, x2);
#ifdef ONLY_PART5
        if( !interp1equid_valid(SOC_table_R0, R0_table_dc, x2) ) {
            result.valid = false;
            return result;
        }
#endif
        R0 = interp1equid(SOC_table_R0, R0_table_dc, x2);
    }

#ifdef ONLY_PART5
    if( !interp2equid_valid(BSG_speed_data, BSG_torque_full, BSG_eff_map_full, w_bsg_avg, o5_T_bsg) ) {
        result.valid = false;
        return result;
    }
#endif
    double eff_bsg = interp2equid(BSG_speed_data, BSG_torque_full, BSG_eff_map_full, w_bsg_avg, o5_T_bsg);

#ifndef PART6
    DUMMY_OUTPUT7(eff_bsg, m_fuel, T_bsg, w_bsg_avg, eff_bsg, R0, Voc)
    return result;
#endif

#endif
#ifdef PART6
    OPAQUE_PART6(o_T_bsg, T_bsg);
    OPAQUE_PART6(o_w_bsg_avg, w_bsg_avg);
    OPAQUE_PART6(o_eff_bsg, eff_bsg);
    OPAQUE_PART6(o_R0, R0);
    OPAQUE_PART6(o_Voc, Voc);

    // Compute BSG power
    double P_dmd_bsg = 0;
    if( o_T_bsg >= 0) {
        P_dmd_bsg = o_T_bsg*o_w_bsg_avg/o_eff_bsg;
    }
    else {
        P_dmd_bsg = o_T_bsg*o_w_bsg_avg*o_eff_bsg;
    }

#ifdef ONLY_PART6
    if(P_dmd_bsg < P_BSG_MIN) {
        result.valid = false;
        return result;
    }
#endif
    P_dmd_bsg = limestone_max(P_dmd_bsg, P_BSG_MIN); // NOTE: Remove when infering bounds is automatic
#ifdef ONLY_PART6
    if(P_dmd_bsg > P_BSG_MAX) {
        result.valid = false;
        return result;
    }
#endif
    P_dmd_bsg = limestone_min(P_dmd_bsg, P_BSG_MAX); // NOTE: Remove when infering bounds is automatic

    // Compute BSG current
    double batt_loss = o_Voc*o_Voc - 4*o_R0*P_dmd_bsg;
#ifdef ONLY_PART6
    if(batt_loss < 0) {
        result.valid = false;
        return result;
    }
#endif
    batt_loss = limestone_max(batt_loss, 0); // NOTE: Remove when infering bounds is automatic

    double I_bsg = (o_Voc - limestone_sqrt(batt_loss))/(2*o_R0);

#ifdef ONLY_PART6
    if(I_bsg > CURR_BSG_MAX) {
        result.valid = false;
        return result;
    }
#endif
    I_bsg = limestone_min(I_bsg, CURR_BSG_MAX); // NOTE: Remove when infering bounds is automatic

#ifndef PART7
    DUMMY_OUTPUT6(I_bsg, m_fuel, v_veh_avg, T_eng, v2, F_brk)
    return result;
#endif

#endif
#ifdef PART7
    OPAQUE_PART7(o_m_fuel, m_fuel);
    OPAQUE_PART7(o_I_bsg, I_bsg);
    OPAQUE_PART7(o_v_veh_avg, v_veh_avg);
    OPAQUE_PART7(o_T_eng, T_eng);
    OPAQUE_PART7(o_v2, v2);
    OPAQUE_PART7(o_F_brk, F_brk);

#ifdef ONLY_PART7
    if(o_I_bsg < CURR_BSG_MIN) {
        result.valid = false;
        return result;
    }
#endif
    o_I_bsg = limestone_max(o_I_bsg, CURR_BSG_MIN); // NOTE: Remove when infering bounds is automatic

    // Update SOC
    result.x2next = x2 - dt*o_I_bsg/(o_v_veh_avg*CAPACITY_CELL);

    // NOTE: This code was previously
    // result.fuel = m_fuel * dt / o_v_veh_avg
    //
    // however, llvm was smart enough to try and vectorize this together with
    // result.time which limestone does not like. To avoid this, we'll pretend
    // to use an intermediate value
    double fuel_pre_veh = o_m_fuel/o_v_veh_avg;
    __invisible_nop(fuel_pre_veh);

    // Return fuel consumed, time, and selected gear
    result.x1next = o_v2;    
    result.fuel = dt*fuel_pre_veh;
    result.time = dt/o_v_veh_avg;
    result.teng = o_T_eng;
    result.fbrk = o_F_brk;

    // Evaluate fuel consumption
    result.stage_cost = dt*(_GAMMA*o_m_fuel + ( 1 - _GAMMA ))/o_v_veh_avg;
    result.valid = true;
    return result;
#endif
}
