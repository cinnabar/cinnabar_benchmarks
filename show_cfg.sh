#!/bin/bash

# set -e pipefail

if [[ "" == "$1" ]]; then
    echo "No ll file specified"
    exit 1;
fi

LL=$1

graph=$(opt -o /dev/null --dot-cfg-only "$LL" 2>&1 | sed "s|Writing '\(.*\)'.*|\1|g")
echo "Building $graph"
dot "$graph" -Tpdf -o graph.pdf
