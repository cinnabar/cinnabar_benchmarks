//#include "omp.h"
// #include <algorithm>

// #include <liblimestone.hpp>

// All defined constants

#define MYINF                   10000

#ifndef __IS_CINNABAR__
#include <math.h>

#define limestone_sqrt sqrt
#define limestone_max fmax
#define limestone_min fmin
#define limestone_abs fabs
#define limestone_trunc (int)


void __invisible_nop(double) {}

#include "interpolation_impl.hpp"

extern "C" {
    __attribute__((optnone)) void __gdb_trampoline() {}
}
#else
#define __gdb_trampoline()
#endif

// Global parameters

// Add ability to set GAMMA as input

#ifndef USE_ORIGINAL_MAPS
#include "maps.hpp"
#include "model_parameters.hpp"
#else
#include "maps_original.hpp"
#include "original_model_parameters.hpp"
#endif

#ifndef HARDINTERP
#include "soft_interp.hpp"
#endif


struct Output {
    double stage_cost;
    double x1next;
    double x2next;
    double fuel;
    double time;
    double teng;
    double fbrk;

    bool valid;
};

// Functions
Output reference_pt_model(
    double x1,
    double x2,
    double u1,
    double u2,
    // NOTE: This used to be an int
    double u3,
    double dt
) {
    // TODO: Set correct bounds
#ifdef __IS_CINNABAR__
    declare_bounds(x1, 0, 1156);
    declare_bounds(x2, 0.3, 0.8);
    declare_bounds(u1, -360, 360);
    declare_bounds(u2, -42, 42);
    declare_bounds(u3, 1, 6);
    declare_bounds(dt, 10, 10);
#endif
    // TODO: Set gamma
    double _GAMMA = 0.3;// = 0.0001;



    // double* rad_s_Trq_bsg_max = limestone_declare_lut({});

    Output result;

    double gear_ratio = 0;
    double w_eng = 0;
    double w_bsg = 0;
    double eff_trans_matrix = 0.99;
    double T_out_ideal = 0;
    double T_out = 0;
    
    double Flb = 0;
    double Fbrk_sat = 0;
    double v2 = 0;
    double v = 0;
    double w_wheel_p = 0;
    double w_eng_p = 0;
    double w_eng_avg = 0;
    double v_veh_avg = 0;
    double w_bsg_avg = 0;
    double P_dmd_bsg = 0;
    double batt_loss = 0;
    double I_bsg = 0;
    
    double F_brk = 0;
    
    double T_pt = 0;
    double T_pt_min = 0;
    double T_pt_max = 0;
    
    double T_bsg = 0;
    double T_eng = 0;
    
    // TODO: Add intrinsic
    double v1 = limestone_sqrt(x1);
    
    // TODO: Add intrinsic
    if( limestone_abs(v1) < 1e-9 ) {
        v1 = 0;
    }
    
    // Wheel speed (rad/s)
    double w_wheel = v1/R_WHEEL;
    
    // Rotational speed at transmission output shaft
    double w_trans_o = w_wheel*FINAL_DRIVE;
    
    // Select gear
    double gear = u3;
    
    // TODO: Check if trunc does what we expect here
    gear_ratio = gear_ratios[ limestone_trunc(gear-1) ];
    // gear_ratio = limestone_
        
    // Calculate engine speed
    w_eng = limestone_max(w_trans_o*gear_ratio, 0);
    /*if( T_pt_tmp < 0 ) {
        w_eng = std::max<double>(w_trans_o*gear_ratio, 0);
    }
    else {
        w_eng = w_trans_o*gear_ratio;
    }*/

    // If engine speed is too high
    if(w_eng > W_ENG_MAX) {
        result.valid = false;
        __gdb_trampoline();
        return result;
    }
    w_eng = limestone_min(w_eng, W_ENG_MAX); // NOTE: Remove when infering bounds is automatic
    // if(w_eng > W_ENG_MAX) return ReferenceResult::W_ENG_OVER_MAX;

    
    // Calculate BSG speed
    w_bsg = TRANS_BSG*w_eng;

    // BSG torque
     T_bsg = u2;
   
    // Calculate max and min bsg torque
    if( !interp1equid_valid(rad_s_Trq_bsg_min, Trq_bsg_min, w_bsg)) {
        result.valid = false;
        __gdb_trampoline();
        return result;
    }
    double T_bsg_min = interp1equid(rad_s_Trq_bsg_min, Trq_bsg_min, w_bsg);

    if( !interp1equid_valid(rad_s_Trq_bsg_max, Trq_bsg_max, w_bsg)) {
        result.valid = false;
        __gdb_trampoline();
        return result;
    }
    double T_bsg_max = interp1equid(rad_s_Trq_bsg_max, Trq_bsg_max, w_bsg);

    
    // Check if T_bsg lies outside of valid range
    if(T_bsg > T_bsg_max) {
        result.valid = false;
        __gdb_trampoline();
        return result;
    }
    T_bsg = limestone_min(T_bsg, T_bsg_max); // NOTE: Remove when infering bounds is automatic
   
    if(T_bsg < T_bsg_min) {
        result.valid = false;
        __gdb_trampoline();
        return result;
    }
    T_bsg = limestone_max(T_bsg, T_bsg_min); // NOTE: Remove when infering bounds is automatic

    // Calculate torque limits
    if( !interp1equid_valid(eng_spd_curve_max_torqe, max_torque_curve, w_eng) ) {
        result.valid = false;
        __gdb_trampoline();
        return result;
    }
    double T_eng_max = interp1equid(eng_spd_curve_max_torqe, max_torque_curve, w_eng);

    // Powertrain torque is limited by what can be provided by engine and BSG motor
    T_pt_min = T_bsg_min*TRANS_BSG;
    T_pt_max = T_bsg_max*TRANS_BSG + T_eng_max;


    T_pt = limestone_max(u1, T_pt_min);
    T_pt = limestone_min(T_pt, T_pt_max);

    // Calculate T_out at wheel
    T_out_ideal = FINAL_DRIVE*gear_ratio*T_pt;

    if( T_out_ideal >= 0)
        T_out = T_out_ideal*eff_trans_matrix;
    else
        T_out = T_out_ideal/eff_trans_matrix;

    // Determine brake force
    // TODO: Add intrinsic
    F_brk = limestone_max( -(u1 - T_pt)*375/15.6, 0); //-(u1 - T_pt)*375/24

    // We cannot brake more than down to 0 m/s
    // TODO: Add intrinsic
    Flb = limestone_max(x1*(MASS/(2*dt) )+T_out/R_WHEEL - A - B_MS*v1 -C_MS*x1, 0);

    // Brake force cannot result in negative velocity
    Fbrk_sat = limestone_min(F_brk,Flb);
    Fbrk_sat = limestone_min(Fbrk_sat,3000);
    Fbrk_sat = limestone_max(Fbrk_sat, 0);

    // Update velocity
    v2 = x1 + ( 2*dt/MASS )*(T_out/R_WHEEL - Fbrk_sat - A - B_MS*v1 -C_MS*x1 );

    
    // T_out is a too large negative number resulting in negative speed
    if( v2 < -1e-9 ) {
        result.valid = false;
        __gdb_trampoline();
        return result;
    }
    v2 = limestone_max(v2, 0 ); // NOTE: Remove when infering bounds is automatic

    // Compute update speed
    
    // To avoid numerical issues with insanely (!) small numbers
    v = limestone_sqrt( v2 );

    // Compute final speed at interval
    w_wheel_p = v/R_WHEEL;
    w_eng_p = w_wheel_p*FINAL_DRIVE*gear_ratio;

    if( w_eng_p > W_ENG_MAX) {
        result.valid = false;
        __gdb_trampoline();
        return result;
    }
    w_eng_p = limestone_min(w_eng_p, W_ENG_MAX); // NOTE: Remove when infering bounds is automatic

    // Calculate average speed
    w_eng_avg = ( w_eng + w_eng_p )/2;
    v_veh_avg = ( limestone_sqrt(x1) + v )/2;

    // Ignore really slow average speeds
    if(v_veh_avg < 0.01) {
        result.valid = false;
        __gdb_trampoline();
        return result;
    }
    v_veh_avg = limestone_max(v_veh_avg, 0.01); // NOTE: Remove when infering bounds is automatic

    // BSG average speed
    w_bsg_avg = TRANS_BSG*w_eng_avg;

    // Compute remaining torque to be provided by engine
    T_eng = T_pt - TRANS_BSG*T_bsg;
    
    if( T_eng > T_eng_max) {
        result.valid = false;
        __gdb_trampoline();
        return result;
    }
    T_eng = limestone_min(T_eng, T_eng_max); // NOTE: Remove when infering bounds is automatic
    T_eng = limestone_max(T_eng, 0);

    // Calculate fuel consumption
    if( !interp2equid_valid(we_list_fuel_rad_s, T_list_fuel_Nm, fuel_map, w_eng_avg, T_eng) ) {
        result.valid = false;
        __gdb_trampoline();
        return result;
    };
    double m_fuel = interp2equid(we_list_fuel_rad_s, T_list_fuel_Nm, fuel_map, w_eng_avg, T_eng);

    double Voc = 0;
    double R0 = 0;
    if(T_bsg <= 0) {
        if( !interp1equid_valid(SOC_table_Voc, Voc_table_ch, x2) ) {
            result.valid = false;
            __gdb_trampoline();
            return result;
        }
        Voc = interp1equid(SOC_table_Voc, Voc_table_ch, x2);
        if( !interp1equid_valid(SOC_table_R0, R0_table_ch, x2) ) {
            result.valid = false;
            __gdb_trampoline();
            return result;
        }
        R0 = interp1equid(SOC_table_R0, R0_table_ch, x2);
        // NOTE: Without this here, LLVM figures out that R0_table_ch and
        // R0_table_dc are just pointers, and converts the code into
        //
        // table_ptr = cond ? R0_table_ch : R0_table_dc R0 =
        // interp1equid(SOC_table_R0, table_ptr, x2).
        //
        // Since the poitner is no longer global, limestone freaks out. By
        // doing a nop on the result in this branch, we prevent this (cool but)
        // useless beiavhour
        __invisible_nop(R0);
    }
    else {
        if( !interp1equid_valid(SOC_table_Voc, Voc_table_dc, x2) ) {
            result.valid = false;
            __gdb_trampoline();
            return result;
        }
        Voc = interp1equid(SOC_table_Voc, Voc_table_dc, x2);
        if( !interp1equid_valid(SOC_table_R0, R0_table_dc, x2) ) {
            result.valid = false;
            __gdb_trampoline();
            return result;
        }
        R0 = interp1equid(SOC_table_R0, R0_table_dc, x2);
    }

    if( !interp2equid_valid(BSG_speed_data, BSG_torque_full, BSG_eff_map_full, w_bsg_avg, T_bsg) ) {
        result.valid = false;
        __gdb_trampoline();
        return result;
    }
    double eff_bsg = interp2equid(BSG_speed_data, BSG_torque_full, BSG_eff_map_full, w_bsg_avg, T_bsg);

    // Compute BSG power
    P_dmd_bsg = 0;
    if( T_bsg >= 0) {
        P_dmd_bsg = T_bsg*w_bsg_avg/eff_bsg;
    }
    else {
        P_dmd_bsg = T_bsg*w_bsg_avg*eff_bsg;
    }

    if(P_dmd_bsg < P_BSG_MIN) {
        result.valid = false;
        __gdb_trampoline();
        return result;
    }
    P_dmd_bsg = limestone_max(P_dmd_bsg, P_BSG_MIN); // NOTE: Remove when infering bounds is automatic
    if(P_dmd_bsg > P_BSG_MAX) {
        result.valid = false;
        __gdb_trampoline();
        return result;
    }
    P_dmd_bsg = limestone_min(P_dmd_bsg, P_BSG_MAX); // NOTE: Remove when infering bounds is automatic

    // Compute BSG current
    batt_loss = Voc*Voc - 4*R0*P_dmd_bsg;
    if(batt_loss < 0) {
        result.valid = false;
        __gdb_trampoline();
        return result;
    }
    batt_loss = limestone_max(batt_loss, 0); // NOTE: Remove when infering bounds is automatic

    I_bsg = (Voc - limestone_sqrt(batt_loss))/(2*R0);

    if(I_bsg > CURR_BSG_MAX) {
        result.valid = false;
        __gdb_trampoline();
        return result;
    }
    I_bsg = limestone_min(I_bsg, CURR_BSG_MAX); // NOTE: Remove when infering bounds is automatic
    if(I_bsg < CURR_BSG_MIN) {
        result.valid = false;
        __gdb_trampoline();
        return result;
    }
    I_bsg = limestone_max(I_bsg, CURR_BSG_MIN); // NOTE: Remove when infering bounds is automatic

    // Update SOC
    result.x2next = x2 - dt*I_bsg/(v_veh_avg*CAPACITY_CELL);

    // NOTE: This code was previously
    // result.fuel = m_fuel * dt / v_veh_avg
    //
    // however, llvm was smart enough to try and vectorize this together with
    // result.time which limestone does not like. To avoid this, we'll pretend
    // to use an intermediate value
    double fuel_pre_veh = m_fuel/v_veh_avg;
    __invisible_nop(fuel_pre_veh);

    // Return fuel consumed, time, and selected gear
    result.x1next = v2;    
    result.fuel = dt*fuel_pre_veh;
    result.time = dt/v_veh_avg;
    result.teng = T_eng;
    result.fbrk = F_brk;

    // Evaluate fuel consumption
    result.stage_cost = dt*(_GAMMA*m_fuel + ( 1 - _GAMMA ))/v_veh_avg;
    result.valid = true;
    __gdb_trampoline();
    return result;
}
