#pragma once

#include <cstddef>

template < typename T, size_t N >
size_t countof( T const (&array)[ N ] )
{
    return N;
}
