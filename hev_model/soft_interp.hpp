#pragma once

template<int N, class T>
consteval inline int countof(T(&)[N]) { return N; }

consteval inline double max_element(const double data[], int N) {
    // Ugly hard coded constant should work fine for now
    double max_elem = -100000000;
    for(int i = 0; i < N; i++) {
        double elem = data[i];
        max_elem = elem > max_elem ? elem : max_elem;
    }
    return max_elem;
}
consteval inline double min_element(const double data[], int N) {
    // Ugly hard coded constant should work fine for now
    double min_elem = 100000000;
    for(int i = 0; i < N; i++) {
        double elem = data[i];
        min_elem = elem < min_elem ? elem : min_elem;
    }
    return min_elem;
}

#define interp1equid(x, y, xp)\
    interp1d_( \
        xp, \
        y, \
        min_element(x, countof(x)), \
        max_element(x, countof(x)), \
        x[1] - x[0] \
    )



__attribute__((always_inline)) inline double interp1d_(
    double xp,
    double const y[],
    double x_min,
    double x_max,
    double dx
) {
    if( (xp < x_min) || (xp >= x_max) ) {
        return y[0];
    }
    else {
        int ii = limestone_trunc( (xp-x_min)/dx ) + 1;
        double x_offset = ((xp-x_min) - (ii-1) * dx);
        return y[ii-1] + (y[ii]-y[ii-1]) *x_offset/dx;
    }
}

// #define interp1equid_valid(x, y, xp) \
//     !( (xp < min_element(x, countof(x))) || (xp >= max_element(x, countof(x))) )


#define interp2equid(x, y, z, xp, yp)\
    limestone_max( \
        limestone_min( \
            interp2d_( \
                z, \
                countof(y), \
                min_element(x, countof(x)), \
                min_element(y, countof(y)), \
                max_element(x, countof(x)), \
                max_element(y, countof(y)), \
                x[1] - x[0], \
                y[1] - y[0], \
                xp, \
                yp \
            ), \
            max_element(z, countof(z)) \
        ), \
        min_element(z, countof(z)) \
    )


__attribute__((always_inline)) inline double interp2d_(
    double const z[],
    unsigned long ny,
    double xmin,
    double ymin,
    double xmax,
    double ymax,
    double dx,
    double dy,
    double xp,
    double yp
) {
    double x_idx = 0;
    double y_idx = 0;

    // TODO: We should probably re-use the valid thing here
    if( xp < xmin || xp >= xmax || yp < ymin || yp >= ymax ) {
        return z[0];
    }
    else {
        x_idx = limestone_trunc( (xp-xmin)/dx ) + 1;
        y_idx = limestone_trunc( (yp-ymin)/dy ) + 1;

        int i11 = limestone_trunc((x_idx-1)*ny + y_idx-1);
        int i12 = limestone_trunc((x_idx-1)*ny + y_idx);
        int i21 = limestone_trunc(x_idx*ny + y_idx-1);
        int i22 = limestone_trunc(x_idx*ny + y_idx);

        double q11 = z[i11];
        double q12 = z[i12];
        double q21 = z[i21];
        double q22 = z[i22];

        // The location of the corners in the x-y plane
        double x_lower = (x_idx - 1) * dx + xmin;
        double y_lower = (y_idx - 1) * dy + ymin;
        double x_upper = x_lower+dx;
        double y_upper = y_lower+dy;

        double x_offset = xp - x_lower;
        double y_offset = yp - y_lower;
        double x_offset_upper = x_upper - xp;
        double y_offset_upper = y_upper - yp;
        return
            ( q11*x_offset_upper*y_offset_upper
            + q21*x_offset*y_offset_upper
            + q12*x_offset_upper*y_offset
            + q22*x_offset*y_offset
            )
            /( dx * dy);
    }
}

#define interp2equid_valid(x, y, z, xp, yp) \
    !( xp < min_element(x, countof(x)) || xp >= max_element(x, countof(x)) || yp < min_element(y, countof(y)) || yp >= max_element(y, countof(y)) )

