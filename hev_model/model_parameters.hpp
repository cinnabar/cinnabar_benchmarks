// Model PARAMETERS
#define R_WHEEL                 0.33
#define FINAL_DRIVE             3.23
#define MASS                    1500
#define TRANS_BSG               2.6000

#define W_ENG_MAX               612.800

#define CAPACITY_CELL           28800
#define CURR_BSG_MAX            256
#define CURR_BSG_MIN            -200
#define P_BSG_MAX               13977 //12000
#define P_BSG_MIN               -16493// -10000

#define Q_LHV                   44600
#define A                       45.9908
#define B_MS                    0.5104
#define C_MS                    0.3686

#define SOC0                    0.60
