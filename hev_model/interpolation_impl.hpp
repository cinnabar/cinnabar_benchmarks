#pragma once

#include <cstddef>
template<size_t XN, size_t YN>
bool interp1equid_valid(double const(&x_map)[XN], double const(&y_map)[YN], double xp) {
    auto xmin = x_map[0];
    auto xmax = x_map[XN-1];
    if( (xp < xmin) || (xp >= xmax)){
        return false;
    }
    return true;
}


template<size_t XN, size_t YN>
double interp1equid(double const(&x_map)[XN], double const(&y_map)[YN], double xp) {
    auto x_min = x_map[0];
    auto x_max = x_map[XN-1];
    auto dx = x_map[1] - x_map[0];

    if( (xp < x_min) || (xp >= x_max) ) {
        return y_map[0];
    }
    else {
        int ii = trunc( (xp-x_min)/dx ) + 1;
        float x_offset = ((xp-x_min) - (ii-1) * dx);
        return y_map[ii-1] + (y_map[ii]-y_map[ii-1]) * x_offset/dx;
    }
}

template<size_t XN, size_t YN, size_t ZN>
bool interp2equid_valid(
    double const(&x_map)[XN],
    double const(&y_map)[YN],
    double const(&z_map)[ZN],
    double xp,
    double yp
) {
    auto xmin = x_map[0];
    auto xmax = x_map[XN-1];
    auto ymin = y_map[0];
    auto ymax = y_map[YN-1];

    if ( (xp < xmin) || (xp > xmax) || (yp < ymin) || (yp > ymax) ) {
        return false;
    }
    return true;
}


template<size_t XN, size_t YN, size_t ZN>
double interp2equid(
    double const(&x_map)[XN],
    double const(&y_map)[YN],
    double const(&z_map)[ZN],
    double xp,
    double yp
) {
    int x_idx = 0;
    int y_idx = 0;

    auto xmin = x_map[0];
    auto xmax = x_map[XN-1];
    auto ymin = y_map[0];
    auto ymax = y_map[YN-1];
    auto dx = x_map[1] - x_map[0];
    auto dy = y_map[1] - y_map[0];

    if( !interp2equid_valid(x_map, y_map, z_map, xp, yp) ) {
        return z_map[0];
    }
    else {
        x_idx = trunc( (xp-xmin)/dx ) + 1;
        y_idx = trunc( (yp-ymin)/dy ) + 1;

        float q11 = z_map[(x_idx-1)*YN + y_idx-1];
        float q12 = z_map[(x_idx-1)*YN + y_idx];
        float q21 = z_map[x_idx*YN + y_idx-1];
        float q22 = z_map[x_idx*YN + y_idx];

        // The location of the corners in the x-y plane
        float x_lower = floor((xp - xmin)/dx) * dx + xmin;
        float y_lower = floor((yp - ymin)/dy) * dy + ymin;
        float x_upper = x_lower+dx;
        float y_upper = y_lower+dy;

        float x_offset = xp - x_lower;
        float y_offset = yp - y_lower;
        float x_offset_upper = x_upper - xp;
        float y_offset_upper = y_upper - yp;
        return
            ( q11*x_offset_upper*y_offset_upper
            + q21*x_offset*y_offset_upper
            + q12*x_offset_upper*y_offset
            + q22*x_offset*y_offset
            )
            /( dx * dy);
    }
}



namespace interp {
    inline bool do_interpolation_valid(ap_fixed<32, 25> x, ap_fixed<32, 25> min, ap_fixed<32, 25> max) {
        return x >= min && x <= max;
    }
}

// bool interp1equid_valid(double const* x_map, double const* y_map, double x) {
//     return true;
// }
// double interp1equid(double const* x_map, double const* y_map, double x) {
//     return 0;
// }
