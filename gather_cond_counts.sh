#!/bin/bash

# Extract the value of a toml variable $1 from file $2
function toml_var {
    local var="$1"
    local filename="$2"
    local result
    result=$( grep "$var" "$filename" \
                | cut -f 2 -d = \
                | perl -pe 's|"([0-9]+)"|\1|g' \
                | tr -d '[:space:]'
            )
    echo "$result"
}

function opt_flags {
    local result
    local metadatafile=${dir}/metadata.toml
    if [ -f "$metadatafile" ]; then
        result=$(toml_var "opt_flags" "$metadatafile" \
                    | tr -d '"' \
                    | tr -d "'"
                )
    fi

    echo "$result"
}

OUT_DIRS=$(find build/* -maxdepth 1 -mindepth 1)

content="name,phi,masks,flags"

while read -r dir; do
    cond_file=$dir/cond_counts.txt

    # Adding package as a hack to work nicely with csv->chart script
    run_name=package_$(basename -- "$dir")

    phi_count=$(grep -e "^Phi count" "$cond_file" \
        | cut -d ":" -f 2
        )

    mask_count=$(grep -e "^Mask count" "$cond_file" \
        | cut -d ":" -f 2
        )

    flags=$(opt_flags)

    content=$(echo -e "$content\n$run_name,$phi_count,$mask_count,$flags")
done <<< "$OUT_DIRS"

echo -e "$content" | sort | column -t -s ',' --output-separator ,
